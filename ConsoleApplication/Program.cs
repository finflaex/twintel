﻿using System;
using System.Collections.Generic;
using ApiMCN;

namespace ConsoleApplication
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            McnService mcnService = new McnService("36880", "83e473096beeb7b0ea25bfb411f3849d");
            
            var countries = mcnService.GetResponse(new NnpCountriesRequest());
            
            foreach (var item in countries)
            {
            
                Console.WriteLine($"code:{item.Code}, name:{item.Name}, nameRus:{item.NameRrus}, prefix:{item.Prefixes}");
            }
            

            Console.ReadLine();
        }
    }
}