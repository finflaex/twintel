﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApiMCN.Services;

namespace ApiMCN
{
    /// <summary>
    ///  Сервис для работы с API MCN Telecom
    /// </summary>
    public class McnService : McnServiceBase
    {
        /// <summary>
        ///  Сервис для работы с API MCN Telecom
        /// </summary>
        /// <param name="accauntId">Номер ЛС</param>
        /// <param name="token">Токен доступа</param>
        public McnService(string accauntId, string token) : base(accauntId, token) { }
        /// <summary>
        /// Получить стоимость звонков
        /// </summary>
        /// <param name="paramRequest">Параметры запроса</param>
        /// <returns></returns>
        public IEnumerable<BillingResponse> GetResponse(BillingRequest paramRequest)
        {
            if (paramRequest.Limit == null)
            {
                return base.ResponseAgregate<BillingRequest, BillingResponse> (paramRequest);
            }
            return base.GetResponse<BillingRequest, BillingResponse>(paramRequest);
        }
        /// <summary>
        ///  Получить список стран
        /// </summary>
        /// <param name="paramRequest">Параметры запроса</param>
        /// <returns></returns>
        public IEnumerable<NnpCountriesResponse> GetResponse(NnpCountriesRequest paramRequest)
            => base.GetResponse<NnpCountriesRequest, NnpCountriesResponse>(paramRequest);
    }
}
