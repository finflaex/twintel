﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiMCN
{
    public interface IResponseAgregateMcn
    {
        int? Limit { get; set; }
        int? Offset { get; set; }
    }
}
