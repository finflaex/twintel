﻿namespace ApiMCN
{
    internal static class Globals
    {
        public const string UrlApiMcn = "https://api.mcn.ru/v2/rest/account/";
        public const string TokenType = "Bearer";
       
        #region Billing
        public const string UrlMethodGetCostCalls = "/did/{0}/billingcdrs.json?year={1}&month={2}&limit={3}";
        public const string UrlMethodGetCostCallsFull = "/did/{0}/billingcdrs?year={1}&month={2}&offset={3}&limit={4}";
        #endregion

        #region NNP
        public const string UrlNnpCountries = "/nnp/get-countries.json";
        public const string UrlNnpCountriesFull = "/nnp/get-countries.json?id={0}";
        #endregion

    }
}
