﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiMCN
{
    public abstract class RequestManagerBase
    {
        public abstract RequestMcn ForRequest(IRequestMcn requestOptions, string accauntId, string token);   

        public string SetAuthParameter(string token)
            => string.Concat(Globals.TokenType, " ", token);

        public string SetUrl(string accauntId, string urlMethod)
            => string.Concat(Globals.UrlApiMcn, accauntId, urlMethod);
    }
}
