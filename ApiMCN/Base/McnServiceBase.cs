﻿using ApiMCN.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApiMCN
{
    public abstract class McnServiceBase
    {                                         
        private readonly string accauntId;
        private readonly string token;

        protected McnServiceBase(string accauntId, string token)
        {
            this.accauntId = accauntId;
            this.token = token;
        }

        protected virtual IEnumerable<TOutput> GetResponse<TInput, TOutput>(TInput paramRequest)
            where TInput : IRequestMcn
            where TOutput : IResponseMcn
        {
            var requestOptions = new RequestManager().ForRequest(paramRequest, accauntId, token);
            var response = WebClientMcn.StartWebClient(requestOptions);
            var result = ResponseManager.ConvertResponse<TOutput>(response);
            return result;
        }

        protected IEnumerable<TOutput> ResponseAgregate<TInput, TOutput>(TInput paramRequest)
            where TInput : IRequestMcn, IResponseAgregateMcn
            where TOutput : IResponseMcn
        {
            const int LIMIT = 1000;
            var offset = paramRequest.Offset ?? 0;
            var responseAgregate = new List<TOutput>();
            paramRequest.Limit = LIMIT;
            while (true)
            {
                paramRequest.Offset = offset;
                var response = GetResponse<TInput, TOutput>(paramRequest).ToList();
                responseAgregate.AddRange(response);
                if (response.Count() < LIMIT)
                {
                    break;
                }
                offset += LIMIT;
            }
            return responseAgregate;
        }
    }
}
