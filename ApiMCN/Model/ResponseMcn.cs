﻿using Newtonsoft.Json; 
using System.Linq;    

namespace ApiMCN
{
    public class ResponseMcn<TOutput>  
        where TOutput: IResponseMcn
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("items")]
        public TOutput[] Items { get; set; }
        [JsonProperty("errors")]
        public ErrorResponseMcn[] Errors { get; set; }
        [JsonProperty("startAt")]
        public int StartAt { get; set; }
        [JsonProperty("maxResult")]
        public int MaxResult { get; set; }
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonIgnore]
        public bool HasError => Errors?.Any() ?? false;
    }      
}