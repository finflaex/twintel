﻿using Newtonsoft.Json; 

namespace ApiMCN
{
    public class ErrorResponseMcn
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }

    }
}
