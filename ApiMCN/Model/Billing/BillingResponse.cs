﻿using System;    
using Newtonsoft.Json;

namespace ApiMCN
{
    public class BillingResponse : IResponseMcn
    {
        public BillingResponse() { }
        public BillingResponse(BillingResponse billingResponse) : base()
        {
            Id = billingResponse.Id;
            ConnectTime = billingResponse.ConnectTime;
            SrcNumber = billingResponse.SrcNumber;
            DstNumber = billingResponse.DstNumber;
            Direction = billingResponse.Direction;
            Length = billingResponse.Length;
            Cost = billingResponse.Cost;
            Rate = billingResponse.Rate;
        }
        /// <summary>
        /// Идентификатор звонка 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// Дата и время начала звонка
        /// </summary>
        [JsonProperty("connect_time")]
        public DateTime ConnectTime { get; set; }
        /// <summary>
        /// Номер, с которого звонили 
        /// </summary>
        [JsonProperty("src_number")]
        public string SrcNumber { get; set; }
        /// <summary>
        /// Номер, на который звонили 
        /// </summary>
        [JsonProperty("dst_number")]
        public string DstNumber { get; set; }
        /// <summary>
        /// Направление звонка
        /// </summary>
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// Время разговора в секундах 
        /// </summary>
        [JsonProperty("length")]
        public long Length { get; set; }
        /// <summary>
        /// Стоимость звонка 
        /// </summary>
        [JsonProperty("cost")]
        public float Cost { get; set; }
        /// <summary>
        /// Стоимость за минуту
        /// </summary>
        [JsonProperty("rate")]
        public float Rate { get; set; }
    }
}
