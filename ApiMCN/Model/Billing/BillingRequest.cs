﻿namespace ApiMCN
{
    /// <summary>
    /// Параметры запроса для получения стоимости звонков
    /// </summary>
    public class BillingRequest : IRequestMcn, IResponseAgregateMcn
    {
        /// <summary>
        /// Параметры запроса для получения стоимости звонков
        /// </summary>
        public BillingRequest()
        { }
        public BillingRequest(BillingRequest billingRequest) : base()
        {
            Number = billingRequest.Number;
            Year = billingRequest.Year;
            Month = billingRequest.Month;
            Limit = billingRequest.Limit;
            Offset = billingRequest.Offset;
        }
        /// <summary>
        /// Номер телефона, для которого выбираем звонки
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Год, за который необходимо получить звонки
        /// </summary>
        public string Year { get; set; }
        /// <summary>
        /// Месяц, за который необходимо получить звонки
        /// </summary>
        public string Month { get; set; }
        /// <summary>
        /// Количество записей в выборке (если не указано выберет все) 
        /// </summary>
        public int? Limit { get; set; }
        /// <summary>
        /// Начало выборки записей, т.е. начиная с какой выбирать записи (значение по умолчанию - 0)
        /// </summary>
        public int? Offset { get; set; }
    }
}
