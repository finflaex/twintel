﻿ namespace ApiMCN
{
    public class RequestMcn
    {
        public string Url { get; set; }
        public string Authorization { get; set; }
        public string Data { get; set; }
        public string HttpMethod { get; set; }
    }
}
