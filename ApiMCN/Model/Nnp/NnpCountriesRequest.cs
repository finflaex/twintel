﻿namespace ApiMCN
{
    /// <summary>
    /// Параметры запроса для получения списка стран
    /// </summary>
    public class NnpCountriesRequest : IRequestMcn
    {
        /// <summary>
        /// ID (не префикс!)
        /// </summary>
        public int? Id { get; set; }
    }
}
