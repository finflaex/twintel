﻿using Newtonsoft.Json;

namespace ApiMCN
{
    public class NnpCountriesResponse : IResponseMcn
    {
        public NnpCountriesResponse() { }
        public NnpCountriesResponse(NnpCountriesResponse nnpCountriesResponse) : base()
        {
            Code = nnpCountriesResponse.Code;
            Name = nnpCountriesResponse.Name;
            NameRrus = nnpCountriesResponse.NameRrus;
            Prefixes = nnpCountriesResponse.Prefixes;
        }

        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("name_rus")]
        public string NameRrus { get; set; }
        [JsonProperty("prefixes")]
        public string Prefixes { get; set; }
    }
}
