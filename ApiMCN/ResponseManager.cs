﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace ApiMCN
{
    internal class ResponseManager
    {
        public static IEnumerable<TOutput> ConvertResponse<TOutput>(string response) where TOutput : IResponseMcn
        {
            var responseOu = JsonConvert.DeserializeObject<ResponseMcn<TOutput>>(response);
            
            if (!responseOu.HasError)
            {    
                return responseOu.Items.AsEnumerable();
            }
            var status = responseOu.Status;
            var code = responseOu.Code.ToString();
            var description = responseOu.Errors.Select(descr => descr.Description).FirstOrDefault();
            throw new Exception($"{status}, code:{code}, описание:{description}");
        }
    }
}
