﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ApiMCN.Services
{
    internal class WebClientMcn
    {
        private long contentLength;
        private const int STARTBIT = 0;

        public static string StartWebClient(RequestMcn requestMcn)
        {
            var request = WebRequest.Create(requestMcn.Url);
            request.Headers.Add(HttpRequestHeader.Authorization, requestMcn.Authorization);
            switch (requestMcn.HttpMethod)
            {
                case "POST":
                    throw new Exception("В работе!");
                case "GET":
                    request.Method = requestMcn.HttpMethod;  
                    string responseMcn = null;
                    try
                    {
                        var response = request.GetResponse();
                        using (var responseStream = response.GetResponseStream())
                        {
                            using (var responseStreamReader = new StreamReader(responseStream))
                            {
                                responseMcn = string.Intern(responseStreamReader.ReadToEnd());     
                            }
                        }
                        response.Close();
                    }
                    catch (Exception e)
                    {                                                                              
                        throw new Exception(e.Message);
                    }
                    return responseMcn;
                default:
                    throw new Exception($"HTTP-метод: {requestMcn.HttpMethod}, не найден!");
            }

        }

        private void SetContentLengtht(string requestParameters)
        {
            contentLength = Encoding.UTF8.GetBytes(requestParameters).Length;
        }

    }
}
