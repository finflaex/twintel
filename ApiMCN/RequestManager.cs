﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiMCN
{
    internal class RequestManager  :RequestManagerBase
    {
        public override RequestMcn ForRequest(IRequestMcn requestOptions, string accauntId, string token)
        {
            switch (requestOptions.GetType().Name)
            {
                case "BillingRequest":
                    return ForRequest((BillingRequest)requestOptions, accauntId, token);
                case "NnpCountriesRequest":
                    return ForRequest((NnpCountriesRequest)requestOptions, accauntId, token);
                default:
                    throw new Exception($"Набор параметров для запроса с именем {requestOptions.GetType().Name}, не найден!");
            }
        }

        public RequestMcn ForRequest(BillingRequest billing, string accauntId, string token)

        {
            var urlMethod = string.Format(Globals.UrlMethodGetCostCallsFull,
                billing.Number,
                billing.Year,
                billing.Month,
                billing.Offset ?? 0,
                billing.Limit ?? 0);
            return new RequestMcn
            {
                Authorization = SetAuthParameter(token),
                HttpMethod = "GET",
                Url = SetUrl(accauntId, urlMethod)
            };
        }

        public RequestMcn ForRequest(NnpCountriesRequest nnpCountries, string accauntId, string token)
        {
            return new RequestMcn
            {
                Authorization = SetAuthParameter(token),
                HttpMethod = "GET",
                Url = SetUrl(accauntId, string.Format(Globals.UrlNnpCountriesFull, nnpCountries.Id))
            };
        }

    }
}
