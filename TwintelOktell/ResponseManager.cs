﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace TwintelOktell
{
    internal class ResponseManager
    {
        internal static IEnumerable<TOutput> ConvertToListStatistic<TOutput>(string responseStrring)
            where TOutput : IStatisticOktell
        {
            var xDocument = XDocument.Parse(responseStrring);
            var serializer = new XmlSerializer(typeof(List<TOutput>), new XmlRootAttribute("root"));
            return (IEnumerable<TOutput>)serializer.Deserialize(xDocument.CreateReader());          
        }
        internal static TOutput ConvertToStatistic<TOutput>(string responseStrring)
            where TOutput : IStatisticOktell
        {
            var xDocument = XDocument.Parse(responseStrring); 
            var serializer = new XmlSerializer(typeof(TOutput));
            return (TOutput)serializer.Deserialize(xDocument.CreateReader());
        }
    }
}
