﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TwintelOktell
{
    internal class WebClientOktell
    {
        public static string StartWebClient(RequestStatistics requestParam)
        {
            var url = GetUrlRequest(requestParam);
            var webClient = new WebClient { Credentials = new NetworkCredential(Globals.Login, Globals.Password) };
            var responseData = webClient.DownloadData(url);
            return GetResponse(responseData);
        }

        private static string GetUrlRequest(RequestStatistics requestParam)
        {
            return string.Concat(Globals.UrlServre,
                string.Format(Globals.UrlStatistic,
                    requestParam.TypeStatistic,
                    requestParam.OktellCode,
                    requestParam.DateFrom.GetString(),
                    requestParam.DateTo.GetString()));
        }

        private static string GetResponse(byte[] dataResponse)
        {
            var responseString = Encoding.UTF8.GetString(dataResponse);
            if (!string.IsNullOrEmpty(responseString))
            {
                if (responseString.Equals("-1"))
                {
                    throw new Exception("При обработке вашего запроса произошла ошибка." +
                                        " Пожалуйста, повторите попытку через несколько минут." +
                                        " Если ошибка повторится, обратитесь в службу технической поддержки");
                }
            }
            else
            {
                throw new Exception("Нет данных по этому запросу");
            }      
            return responseString;
        }
    }
}
