﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class SummaryStatistic : IStatisticOktell, ISummaryStatistic
    {
        [XmlElement("Calls")]
        public CallsStatistic CallsStatistic { private get; set; }
        [XmlElement("Minutes")]
        public MinutesStatistic MinutesStatistic { private get; set; }
        [XmlElement(ElementName = "QualityParams")]
        public QualityParams QualityIndicatorsStatistic { private get; set; }    

        public ICallsStatistic Calls
            => this.CallsStatistic;
        public IMinutesStatistic Minutes
            => this.MinutesStatistic;
        public IQualityIndicatorsStatistic QualityIndicators
            => this.QualityIndicatorsStatistic;
    }
}
