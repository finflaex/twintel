﻿using System; 
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class MinutesStatistic : IMinutesStatistic
    {
        [XmlElement]
        public string SumTimeTalkingIVR { get; set; }
        [XmlElement]
        public string SumTimeTalking { get; set; }
        [XmlElement]
        public string SumTimeTalkingOffice { get; set; }
        [XmlElement]
        public string SumTimeTalkingMobile { get; set; }
    }
}
