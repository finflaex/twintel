﻿using System;
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class DetailedStatistic : IStatisticOktell, IDetailedStatistic
    {
        [XmlElement("direction")]
        public string Direction { get; set; }

        [XmlElement(typeof(DateTime))]
        public DateTime TimeStart { get; set; }

        [XmlElement(typeof(DateTime))]
        public DateTime TimeAnswer { get; set; }

        [XmlElement(typeof(DateTime))]
        public DateTime TimeStop { get; set; }

        [XmlElement]
        public float TimeWaiting { get; set; }

        [XmlElement]
        public float TimeTalking { get; set; }

        [XmlElement]
        public string LineNumber { get; set; }

        [XmlElement]
        public string Operator { get; set; }

        [XmlElement]
        public string SubscriberNumber { get; set; }
    }
}
