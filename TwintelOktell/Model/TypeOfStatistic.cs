﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwintelOktell
{
    internal enum TypeOfStatistic
    {
        Record,
        Summary,
        Detail,
        Transfer
    }
}
