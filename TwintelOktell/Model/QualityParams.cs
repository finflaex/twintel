﻿using System;    
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class QualityParams : IQualityIndicatorsStatistic
    {
        [XmlElement]
        public string TimeTalking { get; set; }

        [XmlElement]
        public string AvgTimeWaiting { get; set; }

        [XmlElement]
        public string ServiceDegree { get; set; }

        [XmlElement]
        public string AvgTimeWaitingForLost { get; set; }

        [XmlElement]
        public string AvgTimeForwarded { get; set; }

        [XmlElement]
        public string SaleCount { get; set; }

        [XmlElement]
        public string SalePercent { get; set; }
    }
}
