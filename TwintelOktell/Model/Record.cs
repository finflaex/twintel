﻿using System;
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class Record: IStatisticOktell, IRecordStatistic
    {
        [XmlElement]
        public string Direction { get; set; }
        [XmlElement]
        public string Group { get; set; }
        [XmlElement]
        public DateTime CallTime { get; set; }
        [XmlElement]
        public string CallingNumber { get; set; }
        [XmlElement]
        public string CalledNumber { get; set; }
        [XmlElement]
        public string WaitingTime { get; set; }
        [XmlElement]
        public string TalkingTime { get; set; }
        [XmlElement]
        public string RecordFileUrl { get; set; }
        [XmlElement]
        public string Comment { get; set; }
    }
}
