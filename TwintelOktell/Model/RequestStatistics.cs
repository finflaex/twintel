﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Threading;

namespace TwintelOktell
{

    public class RequestStatistics : IRequestOktell
    {
        /// <summary>
        /// Параметры запроса
        /// </summary>
        public RequestStatistics() { }
        /// <summary>
        /// Параметры запроса
        /// </summary>
        /// <param name="requestStatistics"></param>
        public RequestStatistics(RequestStatistics requestStatistics) : base()
        {
            OktellCode = requestStatistics.OktellCode;
            DateFrom = requestStatistics.DateFrom;
            DateTo = requestStatistics.DateTo;
        }
        /// <summary>
        /// Код задачи в Oktell
        /// </summary>
        public string OktellCode { get; set; }
        /// <summary>
        /// Дата с
        /// </summary>
        public DateTimeOffset DateFrom { get; set; }
        /// <summary>
        /// Дата по
        /// </summary>
        public DateTimeOffset DateTo { get; set; }
        internal string TypeStatistic { get; private set; }

        internal void SetType(TypeOfStatistic tType)
        {
            var type = tType.ToString();
            TypeStatistic = ResTypeOfStatistic.ResourceManager.GetString(type);
        }
    }
}
