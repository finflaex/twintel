﻿using System; 
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class TransferStatistic : IStatisticOktell, ITransferStatistic
    {
        [XmlElement]
        public string Phone { get; set; }
        [XmlElement]
        public DateTime TimeStart { get; set; }
        [XmlElement]
        public string OperatorTime { get; set; }
        [XmlElement]
        public string TryCount { get; set; }
        [XmlElement]
        public string PartnerTime { get; set; }
    }
}
