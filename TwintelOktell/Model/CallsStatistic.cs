﻿using System;
using System.Xml.Serialization;

namespace TwintelOktell.Model
{
    [Serializable]
    public sealed class CallsStatistic : ICallsStatistic
    {
        [XmlElement]
        public string CountCalls { get; set; }

        [XmlElement]
        public string CountCallsOperators { get; set; }

        [XmlElement]
        public string CountOfficeCalls { get; set; }

        [XmlElement]
        public string CountMobileCalls { get; set; }

        [XmlElement]
        public string CountCallsServedIVR { get; set; }

        [XmlElement]
        public string CountLostCalls { get; set; }
    }
}
