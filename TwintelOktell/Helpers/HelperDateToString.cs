﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TwintelOktell
{
    internal static class HelperDateToString
    {
        public static string GetString(this DateTimeOffset dateTime)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            return dateTime.ToString("yyyy-MM-dd");
        }
    }
}
