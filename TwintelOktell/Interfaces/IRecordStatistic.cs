﻿using System;

namespace TwintelOktell
{
    public interface IRecordStatistic
    {
        string Direction { get; set; }
        string Group { get; set; }
        DateTime CallTime { get; set; }
        string CallingNumber { get; set; }
        string CalledNumber { get; set; }
        string WaitingTime { get; set; }
        string TalkingTime { get; set; }
        string RecordFileUrl { get; set; }
        string Comment { get; set; }
    }
}