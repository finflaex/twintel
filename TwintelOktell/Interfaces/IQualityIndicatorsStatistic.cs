﻿namespace TwintelOktell
{
    public interface IQualityIndicatorsStatistic
    {
        string TimeTalking { get; set; }
        string AvgTimeWaiting { get; set; }
        string ServiceDegree { get; set; }
        string AvgTimeWaitingForLost { get; set; }
        string AvgTimeForwarded { get; set; }
        string SaleCount { get; set; }
        string SalePercent { get; set; }
    }
}