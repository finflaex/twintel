﻿using System;

namespace TwintelOktell
{
    public interface ITransferStatistic
    {
        string Phone { get; set; }
        DateTime TimeStart { get; set; }
        string OperatorTime { get; set; }
        string TryCount { get; set; }
        string PartnerTime { get; set; }
    }
}