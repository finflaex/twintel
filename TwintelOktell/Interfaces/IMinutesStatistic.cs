﻿namespace TwintelOktell
{
    public interface IMinutesStatistic
    {
        string SumTimeTalkingIVR { get; set; }
        string SumTimeTalking { get; set; }
        string SumTimeTalkingOffice { get; set; }
        string SumTimeTalkingMobile { get; set; }
    }
}