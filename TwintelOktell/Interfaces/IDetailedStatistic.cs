﻿using System;

namespace TwintelOktell
{
    /// <summary>
    /// Детализированная звокозаписей
    /// </summary>
    public interface IDetailedStatistic
    {
        string Direction { get; set; }
        DateTime TimeStart { get; set; }
        DateTime TimeAnswer { get; set; }
        DateTime TimeStop { get; set; }
        float TimeWaiting { get; set; }
        float TimeTalking { get; set; }
        string LineNumber { get; set; }
        string Operator { get; set; }
        string SubscriberNumber { get; set; }
    }
}