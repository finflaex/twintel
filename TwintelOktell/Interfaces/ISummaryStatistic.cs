﻿using TwintelOktell.Model;

namespace TwintelOktell
{
    public interface ISummaryStatistic
    {
        ICallsStatistic Calls { get; }
        IMinutesStatistic Minutes { get; }
        IQualityIndicatorsStatistic QualityIndicators { get; }
    }
}