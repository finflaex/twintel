﻿namespace TwintelOktell
{
    public interface ICallsStatistic
    {
        string CountCalls { get; set; }
        string CountCallsOperators { get; set; }
        string CountOfficeCalls { get; set; }
        string CountMobileCalls { get; set; }
        string CountCallsServedIVR { get; set; }
        string CountLostCalls { get; set; }
    }
}