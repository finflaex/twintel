﻿using System;
using System.Collections.Generic;
using System.Text;
using TwintelOktell.Model;

namespace TwintelOktell
{
    /// <summary>
    /// Сервис получения статистик из Oktell
    /// </summary>
    public class OktellStatisticService : OktellStatisticServiceBase
    {
        /// <summary>
        /// Детализированная статистика
        /// </summary>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns></returns>
        public IEnumerable<IDetailedStatistic> DetailedStatistic(RequestStatistics parameters)
        {
            parameters.SetType(TypeOfStatistic.Detail);
            return base.LoadListStatistic<DetailedStatistic>(parameters);
        }
        /// <summary>
        /// Статистика звукозаписей
        /// </summary>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns></returns>
        public IEnumerable<IRecordStatistic> RecordStatistic(RequestStatistics parameters)
        {
            parameters.SetType(TypeOfStatistic.Record);
            return base.LoadListStatistic<Record>(parameters);
        }
        /// <summary>
        /// Статистика переадресации
        /// </summary>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns></returns>
        public IEnumerable<ITransferStatistic> TransferStatistic(RequestStatistics parameters)
        {
            parameters.SetType(TypeOfStatistic.Transfer);
            return base.LoadListStatistic<TransferStatistic>(parameters);
        }
        /// <summary>
        /// Сводная статистика
        /// </summary>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns></returns>
        public ISummaryStatistic SummaryStatistic(RequestStatistics parameters)
        {
            parameters.SetType(TypeOfStatistic.Summary);
            return base.LoadStatistic<SummaryStatistic>(parameters);
        }
    }
}
