﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwintelOktell
{
    public abstract class OktellStatisticServiceBase
    {
        protected IEnumerable<TOut> LoadListStatistic<TOut>(RequestStatistics parameters)
            where TOut :IStatisticOktell
        {
            var statisticString = WebClientOktell.StartWebClient(parameters);
            return ResponseManager.ConvertToListStatistic<TOut>(statisticString);
        }
        protected TOut LoadStatistic<TOut>(RequestStatistics parameters)
            where TOut : IStatisticOktell
        {
            var statisticString = WebClientOktell.StartWebClient(parameters);
            return ResponseManager.ConvertToStatistic<TOut>(statisticString);
        }
    }
}
