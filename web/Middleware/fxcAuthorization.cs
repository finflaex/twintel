﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FinflaexExtension.MVC.Authorization;
using FinflaexExtension.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using TwintelDB;
using TwintelDB.Base;
using TwintelDB.Tables;
using TwintelDB.Types;

namespace web.Middleware
{
    public class fxcAuthorization
    {
        private readonly RequestDelegate _next;

        public fxcAuthorization(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IMemoryCache cache, TWDB db)
        {
            string login = $"{context.Request.Path}{context.Request.QueryString}";
           
            if (!Path.GetExtension(login).IsFree())
            {
                await _next.Invoke(context);
                return;
            }

            if (context.Request.Path != "/base/start/login")
            {
                login = $"/base/start/login?path={login}";
            }

            var routes = context.Request.Path.Value
                .ToLower()
                .Split(new[] {'/', '?'}, StringSplitOptions.RemoveEmptyEntries);
            var area = routes.FirstOrDefault().RetifFree("base");
            var controller = routes.Skip(1).FirstOrDefault().RetifFree("start");
            var action = routes.Skip(2).FirstOrDefault().RetifFree("index");
            var regular = new Regex(@"[^\.]+\.areas\.([^\.]+)\.[^\.]+\.([^\.]+)");
            var type = Assembly
                .GetEntryAssembly()
                .GetTypes()
                .Where(v =>
                {
                    var name = v.FullName.ToLower().RemoveEnd("controller");
                    if (!regular.IsMatch(name)) return false;
                    var matches = regular.Matches(name);
                    return matches[0].Groups[1].Value == area && matches[0].Groups[2].Value == controller;
                })
                .FirstOrDefault();

            var sessionGuid = context.GetSession();

            var session = sessionGuid == null
                ? null
                : db.UserValues.Find(sessionGuid);

            if (session != null)
            {
                if (session.End < DateTimeOffset.Now)
                {
                    session = null;
                }
                else
                {
                    var other = await db.UserValues
                        .Where(v=>v.Type == etUserValue.session)
                        .Where(v => v.Start < DateTimeOffset.Now)
                        .Where(v => v.End > DateTimeOffset.Now)
                        .Where(v => v.Id != session.Id)
                        .Where(v => v.ObjectGuid == session.ObjectGuid)
                        .FirstOrDefaultAsync();

                    if (other != null)
                    {
                        session = null;
                    }
                    else
                    {
                        session.End = DateTimeOffset.Now;
                    }                    
                }
            }
            
            await db.CommitAsync();

            var model = session == null
                ? null
                : await session.Ret(async sess =>
                {
                    var now = DateTimeOffset.Now;
                    var result = await (
                        from user in db.Users
                        where user.Id == sess.ObjectGuid
                        let dynamics = (
                            from dyn in user.Dynamics
                            where dyn.Start < now
                            where dyn.End > now
                            select dyn)
                        select new AuthorizationModel
                        {
                            Id = user.Id,
                            Caption = dynamics
                                .Where(v => v.Type == etUserValue.caption)
                                .Select(v => v.StringValue)
                                .FirstOrDefault(),
                        }
                    ).FirstOrDefaultAsync();

                    result.ActIfNotNull(read =>
                    {
                        read.Roles = db.UserValues
                            .Where(v => v.Start < now && v.End > now)
                            .Where(v => v.ObjectGuid == read.Id)
                            .Where(v => v.Static.Type == etUserStatic.role)
                            .Select(v => v.Static.Key);
                    });

                    return result;
                });

            //var model = cache.GetModel<AuthorizationModel>(sessionGuid);

            if (type != null)
            {
                var method = type.GetMethods().FirstOrDefault(v => v.Name.ToLower() == action);
                if (method != null)
                {
                    var roles = method.GetCustomAttributes().OfType<Roles>().FirstOrDefault()
                                ?? type.GetCustomAttributes().OfType<Roles>().FirstOrDefault();

                    if (model == null && roles != null)
                    {
                        context.Response.Redirect(login);
                        return;
                    }
                    if (model != null && roles != null && roles.Values.Any())
                    {
                        if (!model.Roles.Intersect(roles.Values).Any())
                        {
                            context.Response.Redirect(login);
                            return;
                        }
                    }
                }
            }

            //cache.SetModel(sessionGuid, model);

            if (model != null)
            {
                session = new ctUserValue
                {
                    Start = DateTimeOffset.Now,
                    End = DateTimeOffset.Now.AddMinutes(20),
                    StringValue = $"{context.Request.Path}{context.Request.QueryString}",
                    Type = etUserValue.role,
                    ObjectGuid = model.Id
                };
                sessionGuid = model.Session = session.Id;
                db.Add(session);
            }
            else
            {
                sessionGuid = null;
            }

            await db.CommitAsync();
            
            context.SetSession(sessionGuid);
            
            await _next.Invoke(context);
        }
    }

    public class AuthorizationModel : FxaAuthorization
    {
        public Guid Session { get; set; }
    }

    public static class TokenExtensions
    {
        public static IApplicationBuilder UseFxAuthorization(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<fxcAuthorization>();
        }
    }
}