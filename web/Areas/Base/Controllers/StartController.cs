﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinflaexExtension.MVC.Authorization;
using FinflaexExtension.MVC.Controller;
using FinflaexExtension.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using TwintelDB;
using TwintelDB.Tables;
using TwintelDB.Types;
using web.Middleware;

namespace web.Areas.Base.Controllers
{
    [Area("Base")]
    public class StartController : BaseController<StartController>
    {
        public StartController(IMemoryCache cache) : base(cache)
        {
        }

        public IActionResult InitializeDb()
        {
            return function_result(gb =>
            {
                var user = new ctUser();
                gb.Db.Users.Add(user);
                gb.Db.UserValues.Add(new ctUserValue
                {
                    CreateAuthor = 1.ToGuid(),
                    StringValue = "Система",
                    Type = etUserValue.caption,
                    ObjectGuid = user.Id
                });
                gb.Db.UserValues.Add(new ctUserValue
                {
                    CreateAuthor = 1.ToGuid(),
                    StringValue = "architector",
                    Type = etUserValue.login,
                    ObjectGuid = user.Id
                });
                gb.Db.UserValues.Add(new ctUserValue
                {
                    CreateAuthor = 1.ToGuid(),
                    StringValue = "password".Encrypt(),
                    Type = etUserValue.password,
                    ObjectGuid = user.Id
                });
                gb.Db.UserValues.Add(new ctUserValue
                {
                    CreateAuthor = 1.ToGuid(),
                    ObjectGuid = user.Id,
                    Type = etUserValue.role,
                    Static = new ctUserStatic
                    {
                        Key = "developer",
                        Caption = "разработчик",
                        Type = etUserStatic.role
                    }
                });
                gb.Commit();

                return RedirectToAction("Index", "Start", new {area = "Base"});
            });
        }

        [Roles]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login(string path)
        {
            return View(new LoginModel
            {
                Message = "введите логин и пароль",
                Path = path
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            return await function_result(async gb =>
            {
                if (model.Login.IsFree() || model.Password.IsFree())
                {
                    model.Message = "введите логин и пароль";
                    return Json(model);
                }

                var users = await (
                        from read in gb.Db.UserValues
                        where read.Type == etUserValue.login
                        let now = gb.Now
                        where read.Start < now
                        where read.End > now
                        where read.StringValue == model.Login
                        select read.ObjectGuid
                    )
                    .Intersect(
                        from read in gb.Db.UserValues
                        where read.Type == etUserValue.password
                        let now = gb.Now
                        where read.Start < now
                        where read.End > now
                        where read.StringValue == model.Password
                        select read.ObjectGuid
                    )
                    .ToArrayAsync();

                if (users.Length < 1)
                {
                    model.Message = "логин и пароль не верные";
                    return Json(model);
                }
                if (users.Length > 1)
                {
                    model.Message = "ошибка авторизации - два пользователя с одним логином и паролем.";
                    return Json(model);
                }

                var userId = users.First();

                var otherSession = await gb.Db.UserValues
                    .Where(v => v.Type == etUserValue.session)
                    .Where(v => v.Start < DateTimeOffset.Now)
                    .Where(v => v.End > DateTimeOffset.Now)
                    .FirstOrDefaultAsync(v => v.ObjectGuid == userId);

                if (otherSession != null)
                {
                    var connect = await gb.Db.UserValues
                        .Where(v => v.Type == etUserValue.session)
                        .Where(v => v.Start < DateTimeOffset.Now)
                        .Where(v => v.End > DateTimeOffset.Now)
                        .AnyAsync(v => v.ObjectGuid == userId);

                    if (connect)
                    {
                        model.Message = "ошибка авторизации - имеется незакрытая сессия.";
                        return Json(model);
                    }
                    else
                    {
                        otherSession.End = DateTimeOffset.Now;
                    }
                }

                var requestSession = HttpContext.GetSession();

                var readRequestSession = await (
                        from read in gb.Db.UserValues
                        let now = gb.Now
                        where read.Id == requestSession
                        where read.Type == etUserValue.session
                        where read.Start < now
                        where read.End > now
                        select read
                    )
                    .FirstOrDefaultAsync();

                if (readRequestSession != null)
                {
                    readRequestSession.End = gb.Now;
                }

                var readCurrentSessions = await (
                        from read in gb.Db.UserValues
                        let now = gb.Now
                        where read.ObjectGuid == userId
                        where read.Type == etUserValue.session
                        where read.Start < now
                        where read.End > now
                        select read
                    )
                    .ToArrayAsync();

                foreach (var currentSession in readCurrentSessions)
                {
                    currentSession.End = gb.Now;
                }

                var newSession = new ctUserValue
                {
                    Start = gb.Now,
                    End = gb.Now.AddMinutes(20),
                    ObjectGuid = userId,
                    Type = etUserValue.session
                };

                gb.Add(newSession).Commit();

                var userData = (
                        from read in gb.Db.Users
                        let now = gb.Now
                        where read.Id == userId
                        select new
                        {
                            id = read.Id,
                            caption = (
                                from dyn in read.Dynamics
                                where dyn.Type == etUserValue.caption
                                where dyn.Start < now
                                where dyn.End > now
                                select dyn.StringValue
                            )
                            .FirstOrDefault()
                        }
                    )
                    .First();

                var userRoles = (
                        from read in gb.Db.UserValues
                        let now = gb.Now
                        where read.ObjectGuid == userId
                        where read.Start < now
                        where read.End > now
                        where read.Static.Type == etUserStatic.role
                        select read.Static.Key
                    )
                    .ToArray();

                HttpContext.SetSession(newSession.Id);
                cache.SetModel(newSession.Id, new AuthorizationModel
                {
                    Caption = userData.caption,
                    Roles = userRoles,
                    Id = userData.id,
                    Session = newSession.Id
                });

                model.Path = model.Path
                    .RetIfNull("/")
                    .RetIf("/home/start/login", "/");
                model.Redirect = true;
                return Json(model);
            });
        }


        public async Task<IActionResult> Logout()
        {
            return await function_result(async gb =>
            {
                var readSession = (
                        from old in gb.Db.UserValues
                        where old.Id == Session
                        from session in old.Object.Dynamics
                        where session.Type == etUserValue.session
                        where session.Start < DateTimeOffset.Now
                        where session.End > DateTimeOffset.Now
                        select session)
                    .FirstOrDefault();

                if (readSession != null)
                {
                    readSession.End = gb.Now;
                    await gb.CommitAsync();
                }

                HttpContext.SetSession(null);

                return RedirectToAction("Index", "Start", new
                {
                    area = "Base"
                });
            });
        }


        public class LoginModel
        {
            private string _password = string.Empty;
            public string Login { get; set; }

            public string Password
            {
                get => _password;
                set => _password = value.Encrypt();
            }

            public string Message { get; set; }
            public string Path { get; set; }
            public bool Redirect { get; set; }
        }

        [HttpPost]
        public async Task<IActionResult> Menu()
            => await json_result(async gb =>
            {
                var result = new List<MenuItem>();

                if (ListRoles.Intersect(new[] {"developer", "admin"}).Any())
                {
                    result.AddRange(new[]
                    {
                        new MenuItem
                        {
                            Caption = "Админка",
                            Url = Url.Action("Index", "Start", new {area = "Admin"})
                        }
                    });
                }
                if (ListRoles.Intersect(new[] {""}).Any())
                {
                }

                return result;
            });

        public class MenuItem
        {
            public string Caption { get; set; }
            public string Url { get; set; }
        }
    }
}