﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web.Areas.Base.Controllers
{
    [Area("Base")]
    public class TestController : Controller
    {
        public async Task<IActionResult> GetContacts()
        {
            return Json(new[]{
                new
                {
                    id = Guid.NewGuid().ToString("N"),
                }
            });
        }
    }
}
