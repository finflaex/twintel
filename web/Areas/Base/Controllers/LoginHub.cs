﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FinflaexExtension.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using TwintelDB;
using TwintelDB.Base;
using TwintelDB.Tables;
using TwintelDB.Types;
using web.Areas.AreaSignalR.Controllers;
using web.Middleware;

namespace web.Areas.Base.Controllers
{
    public class LoginHub : BaseConnectHub
    {
        public async Task Login(LoginModel model)
        {
            using (var db = new TWDB())
            {
                if (model.Login.IsFree() || model.Password.IsFree())
                {
                    await ShowMessage("логин или пароль пустые");
                    return;
                }

                var Now = DateTimeOffset.Now;

                var users = await (
                        from read in db.UserValues
                        where read.Type == etUserValue.login
                        let now = Now
                        where read.Start < now
                        where read.End > now
                        where read.StringValue == model.Login
                        select read.ObjectGuid
                    )
                    .Intersect(
                        from read in db.UserValues
                        where read.Type == etUserValue.password
                        let now = Now
                        where read.Start < now
                        where read.End > now
                        where read.StringValue == model.Password
                        select read.ObjectGuid
                    )
                    .ToArrayAsync();

                if (users.Length < 1)
                {
                    await ShowMessage("логин и пароль не верные");
                    return;
                }
                if (users.Length > 1)
                {
                    await ShowMessage("ошибка авторизации - два пользователя с одним логином и паролем.");
                    return;
                }

                var userId = users.First();

                var typeConnect = await db.UserValues
                    .Where(v => v.Type == etUserValue.connect_type)
                    .Where(v => v.Start < DateTimeOffset.Now)
                    .Where(v => v.End > DateTimeOffset.Now)
                    .Select(v => v.StringValue)
                    .FirstOrDefaultAsync();

                var otherSession = await db.UserValues
                    .Where(v => v.Type == etUserValue.session)
                    .Where(v => v.Start < DateTimeOffset.Now)
                    .Where(v => v.End > DateTimeOffset.Now)
                    .FirstOrDefaultAsync(v => v.ObjectGuid == userId);
                
                switch (typeConnect)
                {
                    case "first":
                        if (otherSession != null)
                        {
                            otherSession.End = DateTimeOffset.Now;
                            await db.CommitAsync();
                            await redirect(otherSession.Id, "/base/home/login");
                        }
                        break;
                    default:
                    case "last":
                        if (otherSession != null)
                        {
                            await ShowMessage("ошибка авторизации - имеется незакрытая сессия.");
                            return;
                        }       
                        break;
                }
                
                

//            var requestSession = session;
//
//            var readRequestSession = await (
//                    from read in Db.UserValues
//                    let now = Now
//                    where read.Id == requestSession
//                    where read.Type == deUserValueType.Session
//                    where read.Start < now
//                    where read.End > now
//                    select read
//                )
//                .FirstOrDefaultAsync();
//
//            if (readRequestSession != null)
//            {
//                readRequestSession.End = Now;
//            }
//
//            var readCurrentSessions = await (
//                    from read in Db.UserValues
//                    let now = Now
//                    where read.UserID == userId
//                    where read.Type == deUserValueType.Session
//                    where read.Start < now
//                    where read.End > now
//                    select read
//                )
//                .ToArrayAsync();
//
//            foreach (var currentSession in readCurrentSessions)
//            {
//                currentSession.End = Now;
//            }
//
            var newSession = new ctUserValue
            {
                Id = Session,
                Start = Now,
                //End = Now.AddMinutes(20),
                ObjectGuid = userId,
                Type = etUserValue.session
            };
//
            db.Add(newSession);
            await db.CommitAsync();
            redirect(Session, model.Path);
//
//            var userData = (
//                    from read in Db.Users
//                    let now = Now
//                    where read.Id == userId
//                    select new
//                    {
//                        id = read.Id,
//                        caption = (
//                            from dyn in read.Values
//                            where dyn.Type == deUserValueType.Caption
//                            where dyn.Start < now
//                            where dyn.End > now
//                            select dyn.StringValue
//                        )
//                        .FirstOrDefault()
//                    }
//                )
//                .First();
//
//            var userRoles = (
//                    from read in Db.UserStaticLinks
//                    let now = Now
//                    where read.UserID == userId
//                    where read.Start < now
//                    where read.End > now
//                    where read.Static.Type == deUserStaticType.Role
//                    select read.Static.StringValue
//                )
//                .ToArray();
//
//            HttpContext.SetSession(newSession.Id);
//            cache.SetModel(newSession.Id, new AuthorizationModel
//            {
//                Caption = userData.caption,
//                Roles = userRoles,
//                Id = userData.id,
//                Session = newSession.Id
//            });
//
//            model.Path = model.Path
//                .RetIfNull("/")
//                .RetIf("/home/start/login", "/");
//            model.Redirect = true;
            }
        }

        public async Task ShowMessage(string message)
        {
            await Clients
                .Client(Context.ConnectionId)
                .InvokeAsync("ShowMessage", new[] {message});
        }
    }

    public class LoginModel
    {
        public string Login { get; set; }

        private string _password;

        public string Password
        {
            set => _password = value.Encrypt();
            get => _password;
        }

        public string Message { get; set; }
        public string Path { get; set; }
    }
}