﻿using System;
using System.Linq;
using FinflaexExtension.MVC.Controller;
using Microsoft.Extensions.Caching.Memory;
using TwintelDB;
using TwintelDB.Base;
using TwintelDB.Tables;
using TwintelDB.Types;
using web.Middleware;

namespace web.Areas
{
    public abstract class BaseController<TThis>
        : FxaController<TThis, TWDB, AuthorizationModel>
        where TThis : FxaController<TThis, TWDB, AuthorizationModel>
    {
        public BaseController(IMemoryCache cache) : base(cache)
        {
            SessionToUserGuid = (db, session) =>
            {
                Session = session;
                
                ListRoles = (
                        from value  in db.UserValues
                        let now = DateTimeOffset.Now
                        where value.Id == session
                        from link in value.Object.Dynamics
                        where link.Static.Type == etUserStatic.role
                        where link.Start < now && link.End > now
                        select link.Static.Key)
                    .ToArray();
                
                return db.UserValues
                    .Where(v => v.Id == session)
                    .Where(v => v.Start < DateTimeOffset.Now)
                    .Where(v => v.End > DateTimeOffset.Now)
                    .Where(v => v.Type == etUserValue.session)
                    .Select(v => v.ObjectGuid)
                    .FirstOrDefault();
            };
        }

        protected Guid? Session { get; set; }
        protected string[] ListRoles { get; set; }
    }
}