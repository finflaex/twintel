﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RolesController : BaseController<RolesController>
    {
        public RolesController(IMemoryCache cache) : base(cache)
        {
        }
    }
}