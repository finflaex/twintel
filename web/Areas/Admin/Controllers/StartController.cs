﻿using System.Threading.Tasks;
using FinflaexExtension.MVC.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class StartController: BaseController<StartController>
    {
        public StartController(IMemoryCache cache) : base(cache)
        {
        }

        [Roles("developer", "admin")]
        public async Task<IActionResult> Index()
            => await function_result(async gb =>
            {
                return View();
            });
    }
}