﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FinflaexExtension.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using TwintelDB;
using TwintelDB.Base;
using TwintelDB.Tables;
using TwintelDB.Types;

namespace web.Areas.AreaSignalR.Controllers
{
    public class BaseConnectHub : Hub
    {
        protected Guid Session => Context.ConnectionId.ToGuid();

        public override async Task OnConnectedAsync()
        {
            await writeSession(Session, Guid.Empty);
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var connectionGuid = Context.ConnectionId.ToGuid();

            using (var db = new TWDB())
            {
                var connect = await db.UserValues.FindAsync(connectionGuid);
                if (connect != null)
                {
                    connect.End = DateTimeOffset.Now;
                    await db.CommitAsync();
                }
            }
        }

        public async Task redirect(object connectionId, object value)
        {
            await Clients
                .Client(connectionId.ToString())
                .InvokeAsync("redirect", new[] {value});
        }

        public async Task writeSession(object connectionId, object value)
        {
            await Clients
                .Client(connectionId.ToString())
                .InvokeAsync("writeSession", new[] {value});
        }

        public async Task Connect(string sessionStringId)
        {
            using (var db = new TWDB())
            {
                if (Session.IsEmpty()) return;

                var connectionGuid = Context.ConnectionId.ToGuid();
                var now = DateTimeOffset.Now;

                var userId = await (
                        from session in db.UserValues
                        where session.Id == Session
                        where session.Type == etUserValue.session
                        where (
                                from actual in session.Object.Dynamics
                                where actual.Type == etUserValue.session
                                where actual.Start < now
                                where actual.End > now
                                select actual
                            )
                            .Any()
                        select session.ObjectGuid
                    )
                    .FirstOrDefaultAsync();

                if (userId.IsEmpty()) return;

                var connect = new ctUserValue
                {
                    Id = connectionGuid,
                    Start = now,
                    Type = etUserValue.session,
                    ObjectGuid = userId
                };
                await db.AddAsync(connect);
                await db.CommitAsync();
            }
        }


//        public Task Send(string message) {  
//            return Clients.All.InvokeAsync("Send", $"{Context.ConnectionId}: {message}");  
//        }  
//        public Task SendToGroup(string groupName, string message) {  
//            return Clients.Group(groupName).InvokeAsync("Send", $"{Context.ConnectionId}@{groupName}: {message}");  
//        }  
//        public async Task JoinGroup(string groupName) {  
//            await Groups.AddAsync(Context.ConnectionId, groupName);  
//            await Clients.Group(groupName).InvokeAsync("Send", $"{Context.ConnectionId} joined {groupName}");  
//        }  
//        public async Task LeaveGroup(string groupName) {  
//            await Groups.RemoveAsync(Context.ConnectionId, groupName);  
//            await Clients.Group(groupName).InvokeAsync("Send", $"{Context.ConnectionId} left {groupName}");  
//        }  
//        public Task Echo(string message) {  
//            return Clients.Client(Context.ConnectionId).InvokeAsync("Send", $"{Context.ConnectionId}: {message}");  
//        }  
    }
}