using System.Collections.Generic;
using System.IO.Compression;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using web.Areas.AreaSignalR.Controllers;
using TwintelDB;
using TwintelDB.Base;
using web.Areas.Base.Controllers;
using web.Middleware;

namespace web
{
    public class Startup
    {      
        private IConfigurationRoot _configuration;

        public Startup(IHostingEnvironment env)
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables()
                .Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<TWDB>()
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });
            
            services.Configure<RazorViewEngineOptions>(options => {
                options.ViewLocationExpanders.Add(new MyViewLocationExpander());
            });

            services
                .AddResponseCompression()
                .Configure<GzipCompressionProviderOptions>(options =>
                {
                    options.Level = CompressionLevel.Optimal;
                });

            services.AddSignalR();
            services.AddKendo();
        }

        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            TWDB db,
            IMemoryCache cache)
        {
            app.UseSignalR(routes =>
            {
                routes.MapHub<BaseConnectHub>("connect");
                routes.MapHub<LoginHub>("login");
            });
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            
            app.UseFxAuthorization();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{area=base}/{controller=start}/{action=index}/{id?}/{id2?}/{id3?}");
            });

            app.UseKendo(env);

            app.Run(async context =>
            {
                await context.Response.WriteAsync("not found");
            });
        }
    }
    
    public class MyViewLocationExpander : IViewLocationExpander
    {
        public IEnumerable<string> ExpandViewLocations(
            ViewLocationExpanderContext context, 
            IEnumerable<string> viewLocations)
        {
            yield return "/Areas/{2}/Views/{1}/{0}.cshtml";
            yield return "/Views/Shared/{0}.cshtml";
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {            
        }
    }
}