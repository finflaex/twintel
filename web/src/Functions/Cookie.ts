

// export default class Cookie {
//     static getCookie(name: string): string {
//         const nameLenPlus = (name.length + 1);
//         return document.cookie
//             .split(';')
//             .map(c => c.trim())
//             .filter(cookie => cookie.substring(0, nameLenPlus) === `${name}=`)
//             .map(cookie =>  decodeURIComponent(cookie.substring(nameLenPlus)))
//             [0] || '';
//     }
// }

export class cookie {

    static read(name: string) {
        const result = new RegExp('(?:^|; )' + encodeURIComponent(name) + '=([^;]*)').exec(document.cookie);
        return result ? result[1] : null;
    }

    static write(name: string, value: string, minuts?: number) {
        if (!minuts) {
            minuts = 20;
        }

        const date = new Date();
        date.setTime(date.getTime() + (minuts * 60 * 1000));

        const expires = "; expires=" + date.toUTCString();

        document.cookie = name + "=" + value + expires + "; path=/";
    }

    static remove(name: string) {
        this.write(name, "", -1);
    }

}