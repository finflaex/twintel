﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';

import './client.scss';

// import '@progress/kendo-ui';
// import { Button } from '@progress/kendo-buttons-react-wrapper';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
// import '@progress/kendo-ui/css/web/kendo.material.css';
import '../../kendo.custom.css';

import { Header } from '../../Components/Header/Header';
import { Footer } from '../../Components/Footer/Footer';
import { MenuActions } from '../../Components/MenuActions/MenuActions';
import { ProfilePage } from './ProfilePage/ProfilePage';
import { MenuProjects } from '../../Components/MenuProjects/MenuProjects';

ReactDOM.render(
    <div>
        <Header />
        <div className="flexbox-row">
            <div className="flexbox-col list">
                <MenuProjects />
            </div>
            <div className="flexbox-col list">
                <MenuActions />
            </div>
            <div className="flexbow-col content">
                <ProfilePage />
            </div>
        </div>
        <Footer />
    </div>,
    document.getElementById('root')
);

// todo kendo test
// import * as $ from 'jquery';
// import '@progress/kendo-ui';
// import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js'
// kendo.culture('ru-RU');
// $('#kendo_test').kendoDropDownList();