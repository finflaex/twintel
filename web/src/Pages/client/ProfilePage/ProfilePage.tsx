﻿import * as React from 'react';

import '@progress/kendo-ui';
import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js';
kendo.culture('ru-RU');

import { Button, ButtonClickEvent, ButtonStyle } from '../../../Components/Button/Button';
import { DatePicker } from '@progress/kendo-dateinputs-react-wrapper';
import { DTextBox } from '../../../Components/DTextBox/DTextBox';

import './ProfilePage.scss';
import { TextBox } from "../../../Components/TextBox/TextBox";
import { Gender } from '../../../Components/RadioGroup/Gender/Gender';


class ProfilePage extends React.Component {
    render() {
        return (
            <div>
                <h1>Заполнить профиль</h1>
                <p>Добро пожаловать в личный кабинет TWINTEL. Заполните пожалуйста данные о себе.</p>
                <div className="panel">
                    <div className="row">
                        <div className="col-md-4">
                            <TextBox
                                placeholder={'Фамилия'}
                                label={'Фамилия'}
                            />
                        </div>
                        <div className="col-md-4">
                            <TextBox
                                placeholder={'Имя'}
                                label={'Имя'}
                            />
                        </div>
                        <div className="col-md-4">
                            <TextBox
                                placeholder={'Отчество'}
                                label={'Отчество'}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <label>День рождения</label>
                            {/*<DatePicker/>*/}
                        </div>
                        <div className="col-md-4">
                            <label>Пол</label>
                            <Gender />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <label>Почта</label>
                            <DTextBox
                                placeholder={'Почта'}
                                caption={'+ Добавить почту'}
                                num={1}
                            />
                        </div>
                        <div className="col-md-4">
                            <label>Телефон</label>
                            <DTextBox
                                placeholder={'Телефон'}
                                caption={'+ Добавить телефон'}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <Button
                                style={ButtonStyle.primary}
                                caption={'Сохранить'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// todo создать метод для отправки данных на сервер

export { ProfilePage };