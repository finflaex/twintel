﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Hello from '../../testRedux/containers/Hello';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {enthusiasm} from '../../testRedux/reducers/index';
import {StoreState} from '../../testRedux/types/index';


const store = createStore<StoreState>(enthusiasm, {
    enthusiasmLevel: 1,
    languageName: 'world',
});

ReactDOM.render(
    <div>
        <Provider store={store}>
            <Hello/>
        </Provider>
    </div>,
    document.getElementById('root') as HTMLElement
);