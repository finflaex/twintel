import * as React from 'react';
import {Field, reduxForm} from 'redux-form';
import {TextBox} from "../../Components/TextBox/TextBox";

//const renderField = (data:any) => {
//    return (
//        <div>
//            <TextBox formInput={data.input}/>
//            {/*<label>{data.label}</label>*/}
//            {/*<div>*/}
//            {/*<input */}
//            {/*{...data.input} */}
//            {/*placeholder={data.label} */}
//            {/*type={data.type}*/}
//            {/*/>*/}
//            {/*</div>*/}
//        </div>
//    ) 
//};

class Form extends React.Component<any> {
    renderField = (data: any) => (
        <div>
            <TextBox formInput={data.input} label="Email или телефон" placeholder="Email или телефон" />
        </div>
    );

    render() {
        const {handleSubmit, reset} = this.props;

        const submit = (values: any) => console.log(values);

        return (
            <form onSubmit={handleSubmit(submit)}>
                {/* принимает имя поля, тип и остальные свойства, которые расмотрим позже*/}
                <Field name="login" component={this.renderField} />
                <button type="submit">Войти</button>
            </form>
        );
        
        //<button type={"button"} onClick={this.test.bind(this)}>Test</button>
    }
}

let ReduxForm = reduxForm({form: 'post'})(Form);

export {ReduxForm};