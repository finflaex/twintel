import {createStore, combineReducers, applyMiddleware} from 'redux'
import {reducer as formReducer} from 'redux-form'

const reducers = {
    form: formReducer
};

function logger({ getState }: any) {
    return (next: any) => (action: any) => {
        let returnValue = next(action);

        let model = getState();

        console.log('will dispatch', action);
        console.log('state after dispatch', getState());
        let data = {
            type: action.type.split('\/')[1],
            name: action.meta.field,
            form: action.meta.form,
            value: action.payload
        };
        if (data.name) {
            //console.log(getState().form.post.values);
            console.log(data);
        }

        return returnValue;
    }
}

const reducer = combineReducers(reducers);
const store = createStore(reducer, applyMiddleware(logger) );

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {ReduxForm} from "./ReduxForm";
import {Provider} from "react-redux";

ReactDOM.render(
    <Provider store={store}>
        <ReduxForm/>
    </Provider>,
    document.getElementById('root')
);