﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import './login.scss';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
import '../../kendo.custom.css';

import { connectHub } from "../../Types/Hubs";
import { TabItem } from "../../Components/TabItem/TabItem";
import { TabSheet } from "../../Components/TabSheet/TabSheet";
import { Logo } from "../../Components/Logo/Logo";
import { FormLoginProvider, FormRegMeProvider } from "./provider";

ReactDOM.render(
    <div id="form_authorization">
        <div className="text-center">
            <Logo />
            <h3>Личный кабинет</h3>
        </div>
        <div className="formLogin">
            <TabSheet myChildren={
                [
                    new TabItem('login', 'Вход', <FormLoginProvider />, true),
                    new TabItem('registration', 'Регистрация', <FormRegMeProvider />)
                ]
            } />
        </div>
    </div>,
    document.getElementById('root')
);

connectHub.start();