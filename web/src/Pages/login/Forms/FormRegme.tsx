import * as React from 'react';
import {Field, reduxForm} from 'redux-form';
import {TextBox} from '../../../Components/TextBox/TextBox';
import {Button, ButtonStyle} from '../../../Components/Button/Button';

class Form extends React.Component<any> {
    renderField = ({input, label, type}: any) => (
        <TextBox
            formInput={input}
            label={label}
            placeholder={label}
            type={type}
        />
    );

    render() {
        //const {handleSubmit, reset} = this.props;
        {/*<form onSubmit={handleSubmit(submit)}>*/}
        // const submit = (values: any) => console.log(values);

        return (
            <form>
                <Field name="email" component={this.renderField} label="Email" type={'email'}/>
                <Field name="phone" component={this.renderField} label="Телефон" type="tel"/>
                <Field name="password1" component={this.renderField} label="Пароль" type="password"/>
                <Field name="password2" component={this.renderField} label="Повторите пароль" type="password"/>
                <Button style={ButtonStyle.primary} caption={'Зарегистрироваться'}/>
            </form>
        );
    }
}

let ReduxFormRegMe = reduxForm({form: 'regme'})(Form);

export {ReduxFormRegMe};