import * as React from 'react';
import '../../../Styles.scss';

//import '@progress/kendo-ui';
//import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js';
//kendo.culture('ru-RU');
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../Components/TextBox/TextBox';
import { Button, ButtonStyle } from '../../../Components/Button/Button';
//import { DropDownList } from '@progress/kendo-dropdowns-react-wrapper';

class Form extends React.Component<any> {
    renderField = ({ input, label, type }: any) => (
        <TextBox
            formInput={input}
            label={label}
            placeholder={label}
            password={type == 'password'}
        />
    );

    //renderSelect(data: any) { 
    //    const options = {
    //        name: data.name,
    //        id: data.name,
    //        filter: "startswith",
    //        dataTextField: "ProductName",
    //        dataValueField: "ProductID",
    //        dataSource: {
    //            type: "odata",
    //            serverFiltering: true,
    //            transport: {
    //                read: {
    //                    url: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Products",
    //                }
    //            }
    //        },
    //        select: data.input.onFocus,
    //        change: data.input.onChange,
    //        //close: onClose,
    //        //open: onOpen,
    //        //filtering: onFiltering,
    //        //dataBound: onDataBound
    //    }

    //    console.log(data);
    //    return (
    //    <div>
    //        <label>Компания</label>
    //        <br />
    //        <DropDownList
    //                {...options}
    //        />
    //    </div>
    //);
    //}
    render() {
        const {handleSubmit, reset} = this.props;
        const submit = (values: any) => console.log(values);

        return (
            <form onSubmit={handleSubmit(submit)}>
                <Field name="login" component={this.renderField} label="Email или телефон" type={'text'} />
                <Field name="password" component={this.renderField} label="Пароль" type="password" />
                <div className="dropDownList"> 
                    <label htmlFor="company">Компания</label>
                    <div>
                        <Field name="company" component="select" id="company" className="k-dropdown">
                            <option value="company1">ООО "Ромашка"</option>
                            <option value="company2">ООО "Василёк"</option>
                            <option value="company3">ООО "Рога"</option>
                            <option value="company4">ООО "Копыта"</option>
                        </Field>
                    </div>
                </div>
                {/*<Field name="company2" component={this.renderSelect} />*/}
                <Button style={ButtonStyle.primary} caption={'Войти'} type={'submit'} />
            </form>
        );
    }
}

let ReduxFormLogin = reduxForm({ form: 'login' })(Form);

export { ReduxFormLogin };