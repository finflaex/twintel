import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../Components/TextBox/TextBox';
import { Button, ButtonStyle } from '../../../Components/Button/Button';

//const renderField = (data:any) => {
//    return (
//        <div>
//            <TextBox formInput={data.input}/>
//            {/*<label>{data.label}</label>*/}
//            {/*<div>*/}
//            {/*<input */}
//            {/*{...data.input} */}
//            {/*placeholder={data.label} */}
//            {/*type={data.type}*/}
//            {/*/>*/}
//            {/*</div>*/}
//        </div>
//    ) 
//};

class Form extends React.Component<any> {
    renderField = ({ input, label, type }: any) => (
        <div>
            <TextBox formInput={...input} label={label} placeholder={label} type={type} />
        </div>
    );

    render() {
        const { handleSubmit, reset } = this.props;

        const submit = (values: any) => console.log(values);

        return (
            <form onSubmit={handleSubmit(submit)}>
                <Field name="login" component={this.renderField} label="Email или телефон" />
                <Field name="password" component={this.renderField} label="Пароль" type="password" />
                {/*<button type="submit">Войти</button>*/}
                <Button
                    style={ButtonStyle.primary}
                    caption={'Войти'}
                />
            </form>
        );
    }
}

let ReduxFormLogin = reduxForm({ form: 'post' })(Form);

export { ReduxFormLogin };