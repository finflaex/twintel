﻿import * as React from 'react';
import * as $ from 'jquery';
import {Button, ButtonClickEvent, ButtonStyle} from "../../../Components/Button/Button";
import {TextBox} from "../../../Components/TextBox/TextBox";
import {Link} from "../../../Components/Link/Link";

import { interfaceFormLoginData } from "./ReduxLogin/dataType";
import {interfaceFormLoginSubmit} from "./ReduxLogin/actions";
import { config } from "../../../Global";


export class FormLogin extends React.Component<any> {
    message: string;

    render() {
        return (
            <form>
                {
                    this.props.Message ?
                        <div id={'loginMessage'} className="alert alert-danger">
                            {this.props.Message}
                        </div>
                        : null
                }
                <TextBox
                    id={'login'}
                    placeholder={'Email или телефон'}
                    value={this.props.Login}
                    label={'Email или телефон'}
                    onFocus={this.props.onLoginFocus}
                    onKeyPress={this.props.onLoginPress}
                />
                <TextBox
                    id={'password'}
                    placeholder={'Пароль'}
                    label={'Пароль'}
                    password={true}
                    value={this.props.Password}
                    onFocus={this.props.onPasswordFocus}
                    onKeyPress={this.props.onPasswordPress}
                />
                <Button
                    style={ButtonStyle.primary}
                    caption={'Войти'}
                    onClick={this.props.onSubmit}
                />
                <div className="text-center">
                    <Link caption={'Забыли пароль?'}/>
                </div>
            </form>
        );
    }

    componentDidMount() {
        $('[name="login"]').focus();
    }
}

class LoginModel {
    Login: string;
    Password: string;
    Message: string;
    Path: string;
    Redirect: boolean;
}