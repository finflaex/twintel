﻿import * as $ from 'jquery';
import * as React from 'react';
import {Button, ButtonClickEvent, ButtonStyle} from "../../../Components/Button/Button";
import {TextBox} from "../../../Components/TextBox/TextBox";
import {config} from "../../../Global";

export class FormRegMe extends React.Component {
    message: string;
    render() {
        return (
            <form>
                <div className="alert alert-danger hidden">
                    {this.message}
                </div>
                <TextBox
                    id={'mail'}
                    placeholder={'Email'}
                    label={'Email'}

                />
                <TextBox
                    id={'phone'}
                    placeholder={'Телефон'}
                    label={'Телефон'}
                />
                <TextBox
                    id={'newpass'}
                    placeholder={'Пароль'}
                    password={true}
                    label={'Пароль'}
                />
                <TextBox
                    id={'repass'}
                    placeholder={'Повторите пароль'}
                    password={true}
                    label={'Повторите пароль'}
                />
                <Button
                    style={ButtonStyle.primary}
                    caption={'Зарегистрироваться'}
                />
            </form>
        );
    }

    componentDidMount() {
        $('[name="mail"]').focus();
    }

    //fetchForm(e: ButtonClickEvent) {
    //    e.preventDefault();

    //    let model = new RegMeModel();
    //    model.Mail = $('#mail').val() as string;
    //    model.Phone = $('#phone').val() as string;
    //    model.Password = $('#newpass').val() as string;
    //    model.Repass = $('#repass').val() as string;
    //    model.Path = $('#Path').val() as string;
        
    //    $.ajax({
    //        url: config.url + '/base/start/login',
    //        method: 'post',
    //        contentType: 'application/json; charset=utf-8',
    //        dataType: 'json',
    //        data: JSON.stringify(model),
    //        success: (read: RegMeModel) => {
    //            if (read.Redirect) {
    //                document.location.href = config.url + read.Path;
    //            } else{
    //                $('#regMeMessage').text(read.Message).removeClass('hidden');
    //            }
    //        }
    //    });
    //}
}

class RegMeModel {
    public Mail: string;
    public Phone: string;
    public Login: string;
    public Password: string;
    public Message: string;
    public Path: string;
    public Redirect: boolean;
    public Repass: string;
}