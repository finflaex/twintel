// step 4

import {interfaceFormLoginData} from "./dataType";
import {actionsFormLogin} from "./actions";
import * as constants from "./constants";

export function reducerFormLogin(state: interfaceFormLoginData,
                                 action: actionsFormLogin): interfaceFormLoginData {
    switch (action.type) {
        case constants.AUTH_LOGIN_BTN_SUBMIT:
            return {...state};
        case constants.AUTH_LOGIN_INPUTLOGIN_FOCUS:
            return {
                ...state,
                //Login: action.data.Login
            };
        case constants.AUTH_LOGIN_INPUTPASS_FOCUS:
            return {...state};
        case constants.AUTH_LOGIN_INPUTLOGIN_PRESS:
            let dataImputLogin = {...state};
            let charImputLogin = String.fromCharCode(action.Data);
            console.log(action.Data);
            if (action.Data == 8) {
                console.log(action.Data);
                dataImputLogin.Login = state.Login.substring(0, state.Login.length - 1);
            } else if (
                (action.Data >= 48 && action.Data <= 57)
                || (action.Data >= 65 && action.Data <= 90)
            ) {
                console.log(action.Data);
                dataImputLogin.Login = state.Login + charImputLogin;
            }
            return dataImputLogin;
        case constants.AUTH_LOGIN_INPUTPASS_PRESS:
            let dataImputPass = {...state};
            let charImputPass = String.fromCharCode(action.Data);
            if (action.Data == 8) {
                console.log(action.Data);
                dataImputPass.Password = state.Password.substring(0, state.Password.length - 1);
            } else if (
                (action.Data >= 48 && action.Data <= 57)
                || (action.Data >= 65 && action.Data <= 90)
            ) {
                console.log(action.Data);
                dataImputPass.Password = state.Password + charImputPass;
            }
            return dataImputPass;
        default:
            return state;
    }
}