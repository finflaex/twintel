// step 2
import * as constants from './constants'

// button submit
export interface interfaceFormLoginSubmit {
    type: constants.AUTH_LOGIN_BTN_SUBMIT;
}
export function actionFormLoginSubmit(): interfaceFormLoginSubmit {
    return {
        type: constants.AUTH_LOGIN_BTN_SUBMIT
    };
}

//login focus
export interface interfaceFormLoginLoginFocus {
    type: constants.AUTH_LOGIN_INPUTLOGIN_FOCUS;
}
export function actionFormLoginLoginFocus(): interfaceFormLoginLoginFocus {
    return {
        type: constants.AUTH_LOGIN_INPUTLOGIN_FOCUS
    };
}

// password focus
export interface interfaceFormLoginPasswordFocus {
    type: constants.AUTH_LOGIN_INPUTPASS_FOCUS;
}
export function actionFormLoginPasswordFocus(): interfaceFormLoginPasswordFocus {
    return {
        type: constants.AUTH_LOGIN_INPUTPASS_FOCUS
    };
}

// login press
export interface interfaceFormLoginLoginPress {
    type: constants.AUTH_LOGIN_INPUTLOGIN_PRESS;
    Data: number;
}
export function actionFormLoginLoginPress(Data: number): interfaceFormLoginLoginPress {
    return {
        type: constants.AUTH_LOGIN_INPUTLOGIN_PRESS,
        Data: Data
    };
}

// password press
export interface interfaceFormLoginPasswordPress {
    type: constants.AUTH_LOGIN_INPUTPASS_PRESS;
    Data: number;
}

export function actionFormLoginPasswordPress(Data: number): interfaceFormLoginPasswordPress {
    return {
        type: constants.AUTH_LOGIN_INPUTPASS_PRESS,
        Data: Data
    };
}

export type actionsFormLogin
    = interfaceFormLoginSubmit
    & interfaceFormLoginLoginFocus
    & interfaceFormLoginPasswordFocus
    & interfaceFormLoginLoginPress
    & interfaceFormLoginPasswordPress;