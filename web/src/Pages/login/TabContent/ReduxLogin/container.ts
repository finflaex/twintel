// step 5

import * as actions from './actions'
import { connect, Dispatch } from 'react-redux';
import { interfaceFormLoginData } from "./dataType";
import { FormLogin } from "../FormLogin";

export function mapStateToProps(state: interfaceFormLoginData) {
    return state;
}

export function mapDispatchToProps(dispatch: Dispatch<actions.interfaceFormLoginSubmit>) {
    return {
        onLoginFocus: (e: any) => {
            dispatch(actions.actionFormLoginLoginFocus());
        },
        onPasswordFocus: (e: any) => {
            dispatch(actions.actionFormLoginPasswordFocus());
        },
        onLoginPress: (e: any) => {
            dispatch(actions.actionFormLoginLoginPress(e.keyCode || e.charCode));
        },
        onPasswordPress: (e: any) => {
            dispatch(actions.actionFormLoginPasswordPress(e.keyCode || e.charCode));
        },
        onSubmit: (e:any) => {
            e.preventDefault();
            dispatch(actions.actionFormLoginSubmit());
            return false;
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);

//import { Field, reduxForm } from 'redux-form';
//export default reduxForm({
//    form: 'simple'  // a unique identifier for this form
//})(FormLogin)