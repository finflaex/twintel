﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
import '../../../../kendo.custom.css';
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { ReduxForm } from '../../../testForm/ReduxForm';
import { ReduxFormLogin } from '../ReduxFormLogin';

const reducers = {
    form_login: formReducer
};

function logger({ getState }: any) {
    return (next: any) => (action: any) => {
        let returnValue = next(action);

        let model = getState();

        //console.log('will dispatch', action);
        //console.log('state dispatch', getState());
        let data = {
            type: action.type.split('\/')[1],
            name: action.meta.field,
            form: model.form_login
        };
        switch(data.type)
        {
            case 'SET_SUBMIT_SUCCEEDED':
                if (data.form.post) {
                    console.log(data.form.post.values);
                }
                break;
        }

        return returnValue;
    }
}
const reducer = combineReducers(reducers);
const store = createStore(reducer, applyMiddleware(logger));

export class FormLoginProvider extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ReduxFormLogin />
            </Provider>
        );
    }
}
//connectHub.start();