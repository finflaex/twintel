﻿import * as React from 'react';
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { reducer } from 'redux-form'
import {ReduxFormLogin} from "./Forms/FormLogin";
import {ReduxFormRegMe} from "./Forms/FormRegme";

const reducers = {
    form: reducer,
};

function logger({ getState }: any) {
    return (next: any) => (action: any) => {
        let returnValue = next(action);

        let model = getState();

        //console.log('will dispatch', action);
        //console.log('state dispatch', model);
        let data = {
            type: action.type.split('\/')[1],
            field: action.meta.field,
            form: action.meta.form,
            form_data: model.form[action.meta.form] ? model.form[action.meta.form].values : undefined,
            input_data: action.payload
        };
        console.log(data);

        return returnValue;
    }
}
const combines = combineReducers(reducers);
const store = createStore(combines, applyMiddleware(logger));

export class FormLoginProvider extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ReduxFormLogin />
            </Provider>
        );
    }
}

export class FormRegMeProvider extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ReduxFormRegMe />
            </Provider>
        );
    }
}