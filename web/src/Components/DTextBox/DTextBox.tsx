﻿import * as React from 'react';
import { TextBox } from '../TextBox/TextBox';
import { Button, ButtonClickEvent, ButtonStyle } from '../Button/Button';

import './DTextBox.scss';
import {Guid} from "../../Functions/Guid";
import {config} from "../../Global";

interface DTextBoxProps {
    placeholder?: string;
    caption?: string;
    num?: number;
}

interface DTextBoxState {
    count?: number;
    array: Array<DataTextBox>;
}

class DataTextBox {
    id: string;
}

class DTextBox extends React.Component<DTextBoxProps, DTextBoxState> {
    constructor(props: any) {
        super(props);
        this.state = {array: []}
    }

    //componentDidMount() {
    //    let value = new DataTextBox();
    //    value.id = Guid.newGuid();
    //    let arr = this.state.array;
    //    arr.push(value);
    //    this.setState({ array: arr });
    //}

    componentDidMount() {
        let arr = this.state.array;
        let list = $.ajax({
            url: config.url + '/Base/Test/GetContacts',
            data: JSON.stringify({}),

            method: 'post',
            dataType: 'application/json; charset=utf-8',
            contentType: 'json',
        })
            .done(data => {
                // получили данные с сервера
                console.log(data);
                return data;
            });
        // вывели данные на страницу
        console.log(list);
        //arr.push(list);
        //this.setState({ array: arr });
    }

    handleClose(id: string) {
        let arr = this.state.array;
        let newArray = new Array<DataTextBox>();
        arr.forEach(item => {
            if (item.id !== id)
                newArray.push(item);
        });
        this.setState({ array: newArray })
    }

    appendInput() {
        let value = new DataTextBox();
        value.id = Guid.newGuid();
        let arr = this.state.array;
        arr.push(value);
        this.setState({ array: arr })
    }

    render() {
        let list = this.state.array !== undefined ? this.state.array.map(item => {
            return (
                <div className="dTextBox" id={item.id}>
                    <TextBox
                        placeholder={this.props.placeholder || ''}
                    />
                    <i className="k-icon k-i-close" onClick={this.handleClose.bind(this, item.id)} />
                </div>
            );
        }): [];

        return (
            <div>
                {list}
                <Button
                    caption={this.props.caption || '+ Добавить'}
                    onClick={this.appendInput.bind(this)}
                />
            </div>
        );
    }
}

export { DTextBox };