﻿import * as React from 'react';
import './CheckBox.scss';
import {Guid} from "../../Functions/Guid";

interface CheckBoxProps {
    id?: string;
}

export class CheckBox extends React.Component<CheckBoxProps, {}> {
    render() {
        let elId = this.props.id || Guid.newGuid();
        return (
            <div className="CheckBoxStyle">
                <input
                    type="checkbox"
                    className="k-checkbox"
                    id={elId}
                />
                <label className="k-checkbox-label" htmlFor={elId}>{this.props.children}&nbsp;</label>
            </div>
        );
    }
}