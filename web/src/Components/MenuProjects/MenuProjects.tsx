﻿import * as React from 'react';

import './MenuProjects.scss';
import { Projects } from '../RadioGroup/Projects/Projects';

class MenuProjects extends React.Component {    
    render() {
        return (
            <div className="ProjectsStyle">
                <h5>Проекты</h5>
                <Projects />
            </div>
        );
    }
}

export { MenuProjects };