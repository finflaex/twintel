﻿import * as React from 'react';

import './Link.scss';

interface LinkProps {
    caption?: string;
    link?: string;
}

export class Link extends React.Component<LinkProps, {}> {
    render() {
        return (
            <a
                className={'ButtonLink'}
                children={this.props.caption}
                href={this.props.link}
            />
        );
    }
}