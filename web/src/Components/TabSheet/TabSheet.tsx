import * as React from 'react';
import 'jquery';
import 'bootstrap';
import './TabSheet.scss';

import { TabItem } from '../TabItem/TabItem';
import { List } from 'linqts';
import {Guid} from "../../Functions/Guid";

interface TabSheetProps {
    myChildren?: TabItem[]; 
}

class TabSheet extends React.Component<TabSheetProps, any> {

    myGuid: string;

    constructor(props: TabSheetProps) {
        super(props);
        this.myGuid = Guid.newGuid()
    }

    handleClick(event: any): void {
        let id = event.target.id;
        $('#' + this.myGuid + ' > ul > li').removeClass('active');
        $('#' + id).addClass('active');

        id = event.target.id.replace('tab-', 'content-');
        $('#' + this.myGuid + ' > div').addClass('hidden');
        $('#' + id).removeClass('hidden');
    }

    render() {
        let captions = this.props.myChildren === undefined
            ? null : new List(this.props.myChildren)
                //.Select(v => v === undefined ? '' : v.caption)
                .ToArray()
                .map(v =>
                    <li key={v.key} id={'tab-' + v.name} onClick={this.handleClick.bind(this)} className={v.active ? 'active' : ''}>
                        {v.caption}
                    </li>
                );

        let bodies = this.props.myChildren === undefined
            ? null : this.props.myChildren.map(v =>
                <div key={v.key} id={'content-' + v.name} className={v.active ? '' : 'hidden'}>
                    {v.children}
                </div>
            );

        return (
            <div id={this.myGuid}>
                <ul className="tabs">{captions}</ul>
                {bodies}
            </div>
        );
    }
}

export { TabSheet };