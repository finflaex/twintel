﻿import * as React from 'react';

import './Header.scss';

import { Logo } from '../Logo/Logo';
import { Navigation } from '../Navigation/Navigation';

import {Button, ButtonStyle} from "../Button/Button";
import {config} from "../../Global";

export class Header extends React.Component {
    static ButtonExitClick() {
        document.location.href = config.url + '/base/start/logout';
    }

    render() {
        return (
            <header className="HeaderStyle">
                <div className="navbar navbar-default">
                    <div className="container">
                        <div className="navbar-header hidden-xs hidden-sm">
                            <Logo
                                className={'navbar-brand'}
                            />
                        </div>
                        <div className="navbar-right hidden-xs hidden-sm">
                            <div className="navbar-right navbar-autorization">
                                <Button
                                    style={ButtonStyle.primary}
                                    caption={'Выйти'}
                                    onClick={Header.ButtonExitClick}
                                />
                            </div>
                        </div>
                        <div className="navbar-left hidden-xs hidden-sm">
                            <Navigation />
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}