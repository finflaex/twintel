﻿import * as React from 'react';
import './FooterStyle.scss';

export class Footer extends React.Component {
    render() {
        return (
            <footer className="FooterStyle">
                <div className="container">
                    <div className="row footer-top text-center">
                        <div className="footer-phone col-xs-12 col-sm-4 col-md-4">
                            <p className="hidden-xs"><i className="fa fa-mobile-phone" /> Наши телефоны</p>
                            <p>
                                <a href="tel:+73433020102"> +7 343 302-01-02</a>
                                <br />
                                <a href="tel:+74992130086"> +7 499 213-00-86</a>
                            </p>
                        </div>
                        <div className="footer-address col-xs-12 col-sm-4 col-md-4">
                            <p className="hidden-xs"><i className="fa fa-map-marker" aria-hidden="true" /> Наш адрес</p>
                            <p>
                                <a href="https://yandex.ru/maps/54/yekaterinburg/?mode=search&amp;ll=60.621783%2C56.834033&amp;z=16&amp;text=Call-%D1%86%D0%B5%D0%BD%D1%82%D1%80%20%D0%A2%D0%B2%D0%B8%D0%BD%D1%82%D0%B5%D0%BB&amp;sll=60.621783%2C56.834033&amp;sspn=0.024505%2C0.007646&amp;ol=biz&amp;oid=73805897211">620075, Россия, Екатеринбург ул. Энгельса 36, вход 2, оф. 520</a>
                            </p>
                        </div>
                        <div className="footer-mail col-xs-12 col-sm-4 col-md-4">
                            <p className="hidden-xs"><i className="fa fa-envelope" aria-hidden="true" /> Наш email</p>
                            <p>
                                <a href="mailto:twintel@twintel.ru">twintel@twintel.ru</a>
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 copyright text-center">
                            <p>TWINTEL © 2017. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}