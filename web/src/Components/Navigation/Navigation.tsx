﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as $ from 'jquery';
import {config} from "../../Global";
import {Guid} from "../../Functions/Guid";

interface MenuItem {
    Caption: string;
    Url: string;
}

interface NavigationProps {
    MenuList?: MenuItem[];
}

class Navigation extends React.Component<NavigationProps, any> {
    private ID: string;   
    componentWillMount() {
        this.ID = Guid.newGuid();
    }
    componentDidMount() {
        $.ajax({
            url: config.url + '/base/start/menu',
            method: 'post',
            contentType: 'application/json',
            success: (read: MenuItem[]) => {
                let items = read.map(navItem => (
                    <li key={navItem.Caption}>
                        <a href={navItem.Url}>
                            {navItem.Caption}
                        </a>
                    </li>
                    )
                );
                ReactDOM.render(items, document.getElementById(this.ID));
            }
        });
    }
    
    render() {
        return (
            <ul id={this.ID} className="nav navbar-nav"/>
        );
    }
}

export { Navigation };