﻿import * as React from 'react';
import { Button, ButtonClickEvent, ButtonStyle } from '../../Components/Button/Button';
import './MenuActions.scss';

class MenuActions extends React.Component {
    render() {
        return (
            <div>
                <h5>Действия</h5>
                <Button
                    caption={'Заполнить профиль'}
                />
                <Button
                    caption={'Создать организацию'}
                />
                <Button
                    caption={'Найти организацию'}
                />
            </div>
        );
    }
}

export { MenuActions };