﻿import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { Radio } from '../Radio/Radio';
import { RadioGroup } from '../RadioGroup';

const radioName = 'projects';
let arr = new Array();
arr.push(new Radio('Аудит', radioName, 'a'));
arr.push(new Radio('Звонки', radioName, 'b'));
arr.push(new Radio('Звонки2', radioName, 'c'));

interface ProjectsState {
    projects: Radio[];
}

class Projects extends React.Component<{}, ProjectsState> {
    constructor(props: any) {
        super(props);
        this.state = { projects: arr };
    }

    handleSearch(event: any) {
        let searchQuery = event.target.value.toLowerCase();
        let displayedProjects = arr.filter(e => {
            let searchValue = e.label.toLowerCase();
            return searchValue.indexOf(searchQuery) !== -1;
        });
        this.setState({ projects: displayedProjects });
    }
    render() {
        return (
            <div>
                <input
                    type="text"
                    placeholder="Поиск..."
                    onChange={this.handleSearch}
                />
                <RadioGroup myChildren={this.state.projects} />
            </div>
        );
    }
}

export { Projects };