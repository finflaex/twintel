﻿import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { Guid } from "../../../Functions/Guid";

class Radio {
    public label: string;
    public value: string;
    public name: string;
    public id: string;

    constructor(label: string, name: string, value?: string, id?: string) {
        this.label = label;
        this.name = name;
        this.value = value || '';
        this.id = id || Guid.newGuid();
    }
}

export { Radio };