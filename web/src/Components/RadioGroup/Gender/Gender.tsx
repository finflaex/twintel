﻿import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { RadioGroup } from "../RadioGroup";
import {Radio} from "../Radio/Radio";



export class Gender extends React.Component {
    render() {
        let radioName = 'gender';
        let arr = new Array();
        arr.push(new Radio('Мужчина', radioName, 'male'));
        arr.push(new Radio('Женщина', radioName, 'female'));
        return (
            <RadioGroup myChildren={arr} />
        );
    }
}