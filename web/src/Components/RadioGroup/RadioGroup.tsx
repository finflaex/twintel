﻿import * as React from 'react';
import 'jquery';
import 'bootstrap';

import { Radio } from './Radio/Radio';
import { List } from 'linqts';

interface RadioGroupProps {
    myChildren?: Radio[];
}

class RadioGroup extends React.Component<RadioGroupProps, {}> {
    render() {
        let items = this.props.myChildren === undefined ? null : new List(this.props.myChildren)
            .ToArray()
            .map(item => (
                <div key={item.id}>
                    <input
                        type="radio"
                        className="k-radio"
                        name={item.name}
                        value={item.value}
                        id={item.id}
                    />
                    <label className="k-radio-label" htmlFor={item.id}>{item.label}</label>
                </div>
            )
            );
        return (
            <div>
                {items}
            </div>
        );
    }
}

export { RadioGroup };