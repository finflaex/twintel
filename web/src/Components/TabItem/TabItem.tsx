import * as React from 'react';
import 'jquery';
import 'bootstrap';
import './TabItem.scss';
import { Guid } from '../../Functions/Guid';

class TabItem {
    constructor(name: string, caption: string, body: any, active?: boolean) {
        this.caption = caption;
        this.children = body;
        this.active = active || false;
        this.name = name;
        this.key = Guid.newGuid();
    }
    public key: string;
    public caption: string;
    public children: any;
    public active: boolean;
    public name: string;
}

export { TabItem };