﻿import * as React from 'react';
import './Button.scss';
import { List } from 'linqts';
import { MouseEvent, MouseEventHandler } from 'react';

export enum ButtonStyle {
    default = 0,
    primary = 1,
}

export type ButtonClickEvent = MouseEvent<HTMLButtonElement>;

export interface ButtonProps {
    className?: string;
    caption?: string;
    style?: ButtonStyle;
    onClick?(e: ButtonClickEvent): void;
    type?: string;
}

export class Button extends React.Component<ButtonProps, {}> {
    render() {

        let style
            = this.props.style === ButtonStyle.primary ? 'k-primary'
                : '';

        let classList = new List<string>([
            'k-button',
            this.props.className || '',
            style
        ])
            .Where(v => v !== '')
            .ToArray()
            .join(' ');

        return (
            <div className="ButtonStyle">
                <button
                    type={this.props.type}
                    className={classList}
                    children={this.props.caption || this.props.children}
                    onClick={this.props.onClick}
                />
            </div>
        );
    }
}