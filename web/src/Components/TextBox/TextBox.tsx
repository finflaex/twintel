﻿import * as React from 'react';

import './TextBox.scss';
import {Guid} from "../../Functions/Guid";

interface TextBoxProps {
    name?: string;
    password?: boolean;
    placeholder?: string;
    value?: string;
    id?: string;
    label?: string;
    onFocus?(e: any): void;
    onKeyPress?(e: any): void;
    formInput?: any;
    type?: string;
}

export class TextBox extends React.Component<TextBoxProps, {}> {
    render() {
        let elId = this.props.id || Guid.newGuid();
        return (
            <div className="TextBoxStyle k-widget k-maskedtextbox">              
                <input
                    {...this.props.formInput}
                    id={elId}
                    className="k-textbox"
                    name={this.props.name}
                    type={this.props.type || (this.props.password ? 'password' : 'text')}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                />
                <label htmlFor={elId}>{this.props.label || ''}</label>
            </div>
        );
    }
}