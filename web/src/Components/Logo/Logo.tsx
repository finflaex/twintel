﻿import * as React from 'react';
import {config} from "../../Global";


const logo = require('../../logo.png');

interface LogoProps {
    className?: string;
}

class Logo extends React.Component<LogoProps, {}> {
    render() {
        return (
            <a 
                href={config.url || '/'} 
                className={this.props.className}
            >
                <img src={logo} />
            </a>
        );
    }
}

export { Logo };