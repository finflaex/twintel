export interface IAppConfig {
    url: string;
}

export const config: IAppConfig = {
    url: "http://localhost:15422/api/"
};