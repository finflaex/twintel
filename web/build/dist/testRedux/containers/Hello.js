import Hello from '../components/Hello';
import * as actions from '../actions/';
import { connect } from 'react-redux';
export function mapStateToProps(_a) {
    var enthusiasmLevel = _a.enthusiasmLevel, languageName = _a.languageName;
    return {
        enthusiasmLevel: enthusiasmLevel,
        name: languageName,
    };
}
export function mapDispatchToProps(dispatch) {
    return {
        onIncrement: function () { return dispatch(actions.incrementEnthusiasm()); },
        onDecrement: function () { return dispatch(actions.decrementEnthusiasm()); },
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Hello);
//# sourceMappingURL=Hello.js.map