var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'jquery';
import 'bootstrap';
import './TabSheet.scss';
import { List } from 'linqts';
import { Guid } from "../../Functions/Guid";
var TabSheet = /** @class */ (function (_super) {
    __extends(TabSheet, _super);
    function TabSheet(props) {
        var _this = _super.call(this, props) || this;
        _this.myGuid = Guid.newGuid();
        return _this;
    }
    TabSheet.prototype.handleClick = function (event) {
        var id = event.target.id;
        $('#' + this.myGuid + ' > ul > li').removeClass('active');
        $('#' + id).addClass('active');
        id = event.target.id.replace('tab-', 'content-');
        $('#' + this.myGuid + ' > div').addClass('hidden');
        $('#' + id).removeClass('hidden');
    };
    TabSheet.prototype.render = function () {
        var _this = this;
        var captions = this.props.myChildren === undefined
            ? null : new List(this.props.myChildren)
            .ToArray()
            .map(function (v) {
            return React.createElement("li", { key: v.key, id: 'tab-' + v.name, onClick: _this.handleClick.bind(_this), className: v.active ? 'active' : '' }, v.caption);
        });
        var bodies = this.props.myChildren === undefined
            ? null : this.props.myChildren.map(function (v) {
            return React.createElement("div", { key: v.key, id: 'content-' + v.name, className: v.active ? '' : 'hidden' }, v.children);
        });
        return (React.createElement("div", { id: this.myGuid },
            React.createElement("ul", { className: "tabs" }, captions),
            bodies));
    };
    return TabSheet;
}(React.Component));
export { TabSheet };
//# sourceMappingURL=TabSheet.js.map