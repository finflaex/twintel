var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import './Button.scss';
import { List } from 'linqts';
export var ButtonStyle;
(function (ButtonStyle) {
    ButtonStyle[ButtonStyle["default"] = 0] = "default";
    ButtonStyle[ButtonStyle["primary"] = 1] = "primary";
})(ButtonStyle = ButtonStyle || (ButtonStyle = {}));
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Button.prototype.render = function () {
        var style = this.props.style === ButtonStyle.primary ? 'k-primary'
            : '';
        var classList = new List([
            'k-button',
            this.props.className || '',
            style
        ])
            .Where(function (v) { return v !== ''; })
            .ToArray()
            .join(' ');
        return (React.createElement("div", { className: "ButtonStyle" },
            React.createElement("button", { type: this.props.type, className: classList, children: this.props.caption || this.props.children, onClick: this.props.onClick })));
    };
    return Button;
}(React.Component));
export { Button };
//# sourceMappingURL=Button.js.map