var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import './MenuProjects.scss';
import { Projects } from '../RadioGroup/Projects/Projects';
var MenuProjects = /** @class */ (function (_super) {
    __extends(MenuProjects, _super);
    function MenuProjects() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MenuProjects.prototype.render = function () {
        return (React.createElement("div", { className: "ProjectsStyle" },
            React.createElement("h5", null, "\u041F\u0440\u043E\u0435\u043A\u0442\u044B"),
            React.createElement(Projects, null)));
    };
    return MenuProjects;
}(React.Component));
export { MenuProjects };
//# sourceMappingURL=MenuProjects.js.map