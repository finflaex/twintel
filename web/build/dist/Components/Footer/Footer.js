var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import './FooterStyle.scss';
var Footer = /** @class */ (function (_super) {
    __extends(Footer, _super);
    function Footer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Footer.prototype.render = function () {
        return (React.createElement("footer", { className: "FooterStyle" },
            React.createElement("div", { className: "container" },
                React.createElement("div", { className: "row footer-top text-center" },
                    React.createElement("div", { className: "footer-phone col-xs-12 col-sm-4 col-md-4" },
                        React.createElement("p", { className: "hidden-xs" },
                            React.createElement("i", { className: "fa fa-mobile-phone" }),
                            " \u041D\u0430\u0448\u0438 \u0442\u0435\u043B\u0435\u0444\u043E\u043D\u044B"),
                        React.createElement("p", null,
                            React.createElement("a", { href: "tel:+73433020102" }, " +7 343 302-01-02"),
                            React.createElement("br", null),
                            React.createElement("a", { href: "tel:+74992130086" }, " +7 499 213-00-86"))),
                    React.createElement("div", { className: "footer-address col-xs-12 col-sm-4 col-md-4" },
                        React.createElement("p", { className: "hidden-xs" },
                            React.createElement("i", { className: "fa fa-map-marker", "aria-hidden": "true" }),
                            " \u041D\u0430\u0448 \u0430\u0434\u0440\u0435\u0441"),
                        React.createElement("p", null,
                            React.createElement("a", { href: "https://yandex.ru/maps/54/yekaterinburg/?mode=search&ll=60.621783%2C56.834033&z=16&text=Call-%D1%86%D0%B5%D0%BD%D1%82%D1%80%20%D0%A2%D0%B2%D0%B8%D0%BD%D1%82%D0%B5%D0%BB&sll=60.621783%2C56.834033&sspn=0.024505%2C0.007646&ol=biz&oid=73805897211" }, "620075, \u0420\u043E\u0441\u0441\u0438\u044F, \u0415\u043A\u0430\u0442\u0435\u0440\u0438\u043D\u0431\u0443\u0440\u0433 \u0443\u043B. \u042D\u043D\u0433\u0435\u043B\u044C\u0441\u0430 36, \u0432\u0445\u043E\u0434 2, \u043E\u0444. 520"))),
                    React.createElement("div", { className: "footer-mail col-xs-12 col-sm-4 col-md-4" },
                        React.createElement("p", { className: "hidden-xs" },
                            React.createElement("i", { className: "fa fa-envelope", "aria-hidden": "true" }),
                            " \u041D\u0430\u0448 email"),
                        React.createElement("p", null,
                            React.createElement("a", { href: "mailto:twintel@twintel.ru" }, "twintel@twintel.ru")))),
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-xs-12 copyright text-center" },
                        React.createElement("p", null, "TWINTEL \u00A9 2017. All Rights Reserved."))))));
    };
    return Footer;
}(React.Component));
export { Footer };
//# sourceMappingURL=Footer.js.map