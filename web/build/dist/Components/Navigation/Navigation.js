var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as $ from 'jquery';
import { config } from "../../Global";
import { Guid } from "../../Functions/Guid";
var Navigation = /** @class */ (function (_super) {
    __extends(Navigation, _super);
    function Navigation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Navigation.prototype.componentWillMount = function () {
        this.ID = Guid.newGuid();
    };
    Navigation.prototype.componentDidMount = function () {
        var _this = this;
        $.ajax({
            url: config.url + '/base/start/menu',
            method: 'post',
            contentType: 'application/json',
            success: function (read) {
                var items = read.map(function (navItem) { return (React.createElement("li", { key: navItem.Caption },
                    React.createElement("a", { href: navItem.Url }, navItem.Caption))); });
                ReactDOM.render(items, document.getElementById(_this.ID));
            }
        });
    };
    Navigation.prototype.render = function () {
        return (React.createElement("ul", { id: this.ID, className: "nav navbar-nav" }));
    };
    return Navigation;
}(React.Component));
export { Navigation };
//# sourceMappingURL=Navigation.js.map