import 'jquery';
import 'bootstrap';
import { Guid } from "../../Functions/Guid";
var Radio = /** @class */ (function () {
    function Radio(label, name, value, id) {
        this.label = label;
        this.name = name;
        this.value = value || '';
        this.id = id || Guid.newGuid();
    }
    return Radio;
}());
export { Radio };
//# sourceMappingURL=Radio.js.map