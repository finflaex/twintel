var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import './CheckBox.scss';
import { Guid } from "../../Functions/Guid";
var CheckBox = /** @class */ (function (_super) {
    __extends(CheckBox, _super);
    function CheckBox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckBox.prototype.render = function () {
        var elId = this.props.id || Guid.newGuid();
        return (React.createElement("div", { className: "CheckBoxStyle" },
            React.createElement("input", { type: "checkbox", className: "k-checkbox", id: elId }),
            React.createElement("label", { className: "k-checkbox-label", htmlFor: elId },
                this.props.children,
                "\u00A0")));
    };
    return CheckBox;
}(React.Component));
export { CheckBox };
//# sourceMappingURL=CheckBox.js.map