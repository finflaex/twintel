var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { List } from 'linqts';
var RadioGroup = /** @class */ (function (_super) {
    __extends(RadioGroup, _super);
    function RadioGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RadioGroup.prototype.render = function () {
        var items = this.props.myChildren === undefined ? null : new List(this.props.myChildren)
            .ToArray()
            .map(function (item) { return (React.createElement("div", { key: item.id },
            React.createElement("input", { type: "radio", className: "k-radio", name: item.name, value: item.value, id: item.id }),
            React.createElement("label", { className: "k-radio-label", htmlFor: item.id }, item.label))); });
        return (React.createElement("div", null, items));
    };
    return RadioGroup;
}(React.Component));
export { RadioGroup };
//# sourceMappingURL=RadioGroup.js.map