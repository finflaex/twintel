var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { RadioGroup } from "../RadioGroup";
import { Radio } from "../Radio/Radio";
var Gender = /** @class */ (function (_super) {
    __extends(Gender, _super);
    function Gender() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Gender.prototype.render = function () {
        var radioName = 'gender';
        var arr = new Array();
        arr.push(new Radio('Мужчина', radioName, 'male'));
        arr.push(new Radio('Женщина', radioName, 'female'));
        return (React.createElement(RadioGroup, { myChildren: arr }));
    };
    return Gender;
}(React.Component));
export { Gender };
//# sourceMappingURL=Gender.js.map