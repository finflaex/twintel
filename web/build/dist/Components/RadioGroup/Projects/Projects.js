var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'jquery';
import 'bootstrap';
import { Radio } from '../Radio/Radio';
import { RadioGroup } from '../RadioGroup';
var radioName = 'projects';
var arr = new Array();
arr.push(new Radio('Аудит', radioName, 'a'));
arr.push(new Radio('Звонки', radioName, 'b'));
arr.push(new Radio('Звонки2', radioName, 'c'));
var Projects = /** @class */ (function (_super) {
    __extends(Projects, _super);
    function Projects(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { projects: arr };
        return _this;
    }
    Projects.prototype.handleSearch = function (event) {
        var searchQuery = event.target.value.toLowerCase();
        var displayedProjects = arr.filter(function (e) {
            var searchValue = e.label.toLowerCase();
            return searchValue.indexOf(searchQuery) !== -1;
        });
        this.setState({ projects: displayedProjects });
    };
    Projects.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("input", { type: "text", placeholder: "Поиск...", onChange: this.handleSearch }),
            React.createElement(RadioGroup, { myChildren: this.state.projects })));
    };
    return Projects;
}(React.Component));
export { Projects };
//# sourceMappingURL=Projects.js.map