var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import * as React from 'react';
import './TextBox.scss';
import { Guid } from "../../Functions/Guid";
var TextBox = /** @class */ (function (_super) {
    __extends(TextBox, _super);
    function TextBox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextBox.prototype.render = function () {
        var elId = this.props.id || Guid.newGuid();
        return (React.createElement("div", { className: "TextBoxStyle k-widget k-maskedtextbox" },
            React.createElement("input", __assign({}, this.props.formInput, { id: elId, className: "k-textbox", name: this.props.name, type: this.props.type || (this.props.password ? 'password' : 'text'), placeholder: this.props.placeholder, value: this.props.value })),
            React.createElement("label", { htmlFor: elId }, this.props.label || '')));
    };
    return TextBox;
}(React.Component));
export { TextBox };
//# sourceMappingURL=TextBox.js.map