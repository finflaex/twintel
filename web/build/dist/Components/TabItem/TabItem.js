import 'jquery';
import 'bootstrap';
import './TabItem.scss';
import { Guid } from '../../Functions/Guid';
var TabItem = /** @class */ (function () {
    function TabItem(name, caption, body, active) {
        this.caption = caption;
        this.children = body;
        this.active = active || false;
        this.name = name;
        this.key = Guid.newGuid();
    }
    return TabItem;
}());
export { TabItem };
//# sourceMappingURL=TabItem.js.map