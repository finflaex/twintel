var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import './Header.scss';
import { Logo } from '../Logo/Logo';
import { Navigation } from '../Navigation/Navigation';
import { Button, ButtonStyle } from "../Button/Button";
import { config } from "../../Global";
var Header = /** @class */ (function (_super) {
    __extends(Header, _super);
    function Header() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Header.ButtonExitClick = function () {
        document.location.href = config.url + '/base/start/logout';
    };
    Header.prototype.render = function () {
        return (React.createElement("header", { className: "HeaderStyle" },
            React.createElement("div", { className: "navbar navbar-default" },
                React.createElement("div", { className: "container" },
                    React.createElement("div", { className: "navbar-header hidden-xs hidden-sm" },
                        React.createElement(Logo, { className: 'navbar-brand' })),
                    React.createElement("div", { className: "navbar-right hidden-xs hidden-sm" },
                        React.createElement("div", { className: "navbar-right navbar-autorization" },
                            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Выйти', onClick: Header.ButtonExitClick }))),
                    React.createElement("div", { className: "navbar-left hidden-xs hidden-sm" },
                        React.createElement(Navigation, null))))));
    };
    return Header;
}(React.Component));
export { Header };
//# sourceMappingURL=Header.js.map