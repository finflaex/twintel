var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { TextBox } from '../TextBox/TextBox';
import { Button } from '../Button/Button';
import './DTextBox.scss';
import { Guid } from "../../Functions/Guid";
import { config } from "../../Global";
var DataTextBox = /** @class */ (function () {
    function DataTextBox() {
    }
    return DataTextBox;
}());
var DTextBox = /** @class */ (function (_super) {
    __extends(DTextBox, _super);
    function DTextBox(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { array: [] };
        return _this;
    }
    //componentDidMount() {
    //    let value = new DataTextBox();
    //    value.id = Guid.newGuid();
    //    let arr = this.state.array;
    //    arr.push(value);
    //    this.setState({ array: arr });
    //}
    DTextBox.prototype.componentDidMount = function () {
        var arr = this.state.array;
        var list = $.ajax({
            url: config.url + '/Base/Test/GetContacts',
            data: JSON.stringify({}),
            method: 'post',
            dataType: 'application/json; charset=utf-8',
            contentType: 'json',
        })
            .done(function (data) {
            // получили данные с сервера
            console.log(data);
            return data;
        });
        // вывели данные на страницу
        console.log(list);
        //arr.push(list);
        //this.setState({ array: arr });
    };
    DTextBox.prototype.handleClose = function (id) {
        var arr = this.state.array;
        var newArray = new Array();
        arr.forEach(function (item) {
            if (item.id !== id)
                newArray.push(item);
        });
        this.setState({ array: newArray });
    };
    DTextBox.prototype.appendInput = function () {
        var value = new DataTextBox();
        value.id = Guid.newGuid();
        var arr = this.state.array;
        arr.push(value);
        this.setState({ array: arr });
    };
    DTextBox.prototype.render = function () {
        var _this = this;
        var list = this.state.array !== undefined ? this.state.array.map(function (item) {
            return (React.createElement("div", { className: "dTextBox", id: item.id },
                React.createElement(TextBox, { placeholder: _this.props.placeholder || '' }),
                React.createElement("i", { className: "k-icon k-i-close", onClick: _this.handleClose.bind(_this, item.id) })));
        }) : [];
        return (React.createElement("div", null,
            list,
            React.createElement(Button, { caption: this.props.caption || '+ Добавить', onClick: this.appendInput.bind(this) })));
    };
    return DTextBox;
}(React.Component));
export { DTextBox };
//# sourceMappingURL=DTextBox.js.map