var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Button } from '../../Components/Button/Button';
import './MenuActions.scss';
var MenuActions = /** @class */ (function (_super) {
    __extends(MenuActions, _super);
    function MenuActions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MenuActions.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("h5", null, "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044F"),
            React.createElement(Button, { caption: 'Заполнить профиль' }),
            React.createElement(Button, { caption: 'Создать организацию' }),
            React.createElement(Button, { caption: 'Найти организацию' })));
    };
    return MenuActions;
}(React.Component));
export { MenuActions };
//# sourceMappingURL=MenuActions.js.map