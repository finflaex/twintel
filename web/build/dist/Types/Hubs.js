// import {HubConnection} from "@aspnet/signalr-client";
//
// export class Hubs extends HubConnection {
//     initialize() {
//         this.on('redirect', (data: string) => {
//             document.location.href = config.url + data;
//         });
//         this.on('writeSession', (data: string) => {
//             cookie.write('session', data, 20);
//         });
//         return this;
//     }
// }
import { config } from "../Global";
import { cookie } from "../Functions/Cookie";
connectHub.on('redirect', function (data) {
    document.location.href = config.url + data;
});
connectHub.on('writeSession', function (data) {
    cookie.write('session', data, 20);
});
// connectHub.on('blablabla', (data: any) => {
//    store.dispatch({
//        type: 'AUTH_LOGIN_INPUTLOGIN_PRESS',
//        data: data
//    });
// });
export { connectHub };
//# sourceMappingURL=Hubs.js.map