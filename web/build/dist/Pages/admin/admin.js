import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './admin.scss';
//import '@progress/kendo-ui';
// import { Button } from '@progress/kendo-buttons-react-wrapper';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
// import '@progress/kendo-ui/css/web/kendo.material.css';
import '../../kendo.custom.css';
import { Header } from '../../Components/Header/Header';
import { Footer } from '../../Components/Footer/Footer';
ReactDOM.render(React.createElement(Header, null), document.getElementById('pageHeader'));
ReactDOM.render(React.createElement(Footer, null), document.getElementById('pageFooter'));
//# sourceMappingURL=admin.js.map