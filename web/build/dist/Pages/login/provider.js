var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer } from 'redux-form';
import { ReduxFormLogin } from "./Forms/FormLogin";
import { ReduxFormRegMe } from "./Forms/FormRegme";
var reducers = {
    form: reducer,
};
function logger(_a) {
    var getState = _a.getState;
    return function (next) { return function (action) {
        var returnValue = next(action);
        var model = getState();
        //console.log('will dispatch', action);
        //console.log('state dispatch', model);
        var data = {
            type: action.type.split('\/')[1],
            field: action.meta.field,
            form: action.meta.form,
            form_data: model.form[action.meta.form] ? model.form[action.meta.form].values : undefined,
            input_data: action.payload
        };
        console.log(data);
        return returnValue;
    }; };
}
var combines = combineReducers(reducers);
var store = createStore(combines, applyMiddleware(logger));
var FormLoginProvider = /** @class */ (function (_super) {
    __extends(FormLoginProvider, _super);
    function FormLoginProvider() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormLoginProvider.prototype.render = function () {
        return (React.createElement(Provider, { store: store },
            React.createElement(ReduxFormLogin, null)));
    };
    return FormLoginProvider;
}(React.Component));
export { FormLoginProvider };
var FormRegMeProvider = /** @class */ (function (_super) {
    __extends(FormRegMeProvider, _super);
    function FormRegMeProvider() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormRegMeProvider.prototype.render = function () {
        return (React.createElement(Provider, { store: store },
            React.createElement(ReduxFormRegMe, null)));
    };
    return FormRegMeProvider;
}(React.Component));
export { FormRegMeProvider };
//# sourceMappingURL=provider.js.map