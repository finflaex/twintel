var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
import '../../../../kendo.custom.css';
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { ReduxFormLogin } from '../ReduxFormLogin';
var reducers = {
    form_login: formReducer
};
function logger(_a) {
    var getState = _a.getState;
    return function (next) { return function (action) {
        var returnValue = next(action);
        var model = getState();
        //console.log('will dispatch', action);
        //console.log('state dispatch', getState());
        var data = {
            type: action.type.split('\/')[1],
            name: action.meta.field,
            form: model.form_login
        };
        switch (data.type) {
            case 'SET_SUBMIT_SUCCEEDED':
                if (data.form.post) {
                    console.log(data.form.post.values);
                }
                break;
        }
        return returnValue;
    }; };
}
var reducer = combineReducers(reducers);
var store = createStore(reducer, applyMiddleware(logger));
var FormLoginProvider = /** @class */ (function (_super) {
    __extends(FormLoginProvider, _super);
    function FormLoginProvider() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormLoginProvider.prototype.render = function () {
        return (React.createElement(Provider, { store: store },
            React.createElement(ReduxFormLogin, null)));
    };
    return FormLoginProvider;
}(React.Component));
export { FormLoginProvider };
//connectHub.start(); 
//# sourceMappingURL=FormLoginProvider.js.map