// step 4
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import * as constants from "./constants";
export function reducerFormLogin(state, action) {
    switch (action.type) {
        case constants.AUTH_LOGIN_BTN_SUBMIT:
            return __assign({}, state);
        case constants.AUTH_LOGIN_INPUTLOGIN_FOCUS:
            return __assign({}, state);
        case constants.AUTH_LOGIN_INPUTPASS_FOCUS:
            return __assign({}, state);
        case constants.AUTH_LOGIN_INPUTLOGIN_PRESS:
            var dataImputLogin = __assign({}, state);
            var charImputLogin = String.fromCharCode(action.Data);
            console.log(action.Data);
            if (action.Data == 8) {
                console.log(action.Data);
                dataImputLogin.Login = state.Login.substring(0, state.Login.length - 1);
            }
            else if ((action.Data >= 48 && action.Data <= 57)
                || (action.Data >= 65 && action.Data <= 90)) {
                console.log(action.Data);
                dataImputLogin.Login = state.Login + charImputLogin;
            }
            return dataImputLogin;
        case constants.AUTH_LOGIN_INPUTPASS_PRESS:
            var dataImputPass = __assign({}, state);
            var charImputPass = String.fromCharCode(action.Data);
            if (action.Data == 8) {
                console.log(action.Data);
                dataImputPass.Password = state.Password.substring(0, state.Password.length - 1);
            }
            else if ((action.Data >= 48 && action.Data <= 57)
                || (action.Data >= 65 && action.Data <= 90)) {
                console.log(action.Data);
                dataImputPass.Password = state.Password + charImputPass;
            }
            return dataImputPass;
        default:
            return state;
    }
}
//# sourceMappingURL=reducer.js.map