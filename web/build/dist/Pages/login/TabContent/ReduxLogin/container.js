// step 5
import * as actions from './actions';
import { connect } from 'react-redux';
import { FormLogin } from "../FormLogin";
export function mapStateToProps(state) {
    return state;
}
export function mapDispatchToProps(dispatch) {
    return {
        onLoginFocus: function (e) {
            dispatch(actions.actionFormLoginLoginFocus());
        },
        onPasswordFocus: function (e) {
            dispatch(actions.actionFormLoginPasswordFocus());
        },
        onLoginPress: function (e) {
            dispatch(actions.actionFormLoginLoginPress(e.keyCode || e.charCode));
        },
        onPasswordPress: function (e) {
            dispatch(actions.actionFormLoginPasswordPress(e.keyCode || e.charCode));
        },
        onSubmit: function (e) {
            e.preventDefault();
            dispatch(actions.actionFormLoginSubmit());
            return false;
        }
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
//import { Field, reduxForm } from 'redux-form';
//export default reduxForm({
//    form: 'simple'  // a unique identifier for this form
//})(FormLogin) 
//# sourceMappingURL=container.js.map