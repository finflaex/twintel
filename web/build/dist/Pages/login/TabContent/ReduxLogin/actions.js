// step 2
import * as constants from './constants';
export function actionFormLoginSubmit() {
    return {
        type: constants.AUTH_LOGIN_BTN_SUBMIT
    };
}
export function actionFormLoginLoginFocus() {
    return {
        type: constants.AUTH_LOGIN_INPUTLOGIN_FOCUS
    };
}
export function actionFormLoginPasswordFocus() {
    return {
        type: constants.AUTH_LOGIN_INPUTPASS_FOCUS
    };
}
export function actionFormLoginLoginPress(Data) {
    return {
        type: constants.AUTH_LOGIN_INPUTLOGIN_PRESS,
        Data: Data
    };
}
export function actionFormLoginPasswordPress(Data) {
    return {
        type: constants.AUTH_LOGIN_INPUTPASS_PRESS,
        Data: Data
    };
}
//# sourceMappingURL=actions.js.map