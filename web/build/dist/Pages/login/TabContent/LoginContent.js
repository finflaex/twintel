var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as $ from 'jquery';
import { Global } from "../../../Global";
import { Button, ButtonStyle } from "../../../Components/Button/Button";
import { TextBox } from "../../../Components/TextBox/TextBox";
import { Link } from "../../../Components/Link/Link";
var LoginContent = /** @class */ (function (_super) {
    __extends(LoginContent, _super);
    function LoginContent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginContent.prototype.render = function () {
        return (React.createElement("form", null,
            React.createElement("div", { id: 'loginMessage', className: "alert alert-danger hidden" }, this.message),
            React.createElement(TextBox, { id: 'login', placeholder: 'Email или телефон' }),
            React.createElement(TextBox, { id: 'password', placeholder: 'Пароль', password: true }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Войти', onClick: this.fetchForm }),
            React.createElement("div", { className: "text-center" },
                React.createElement(Link, { caption: 'Забыли пароль?' }))));
    };
    LoginContent.prototype.componentDidMount = function () {
        $('[name="login"]').focus();
    };
    LoginContent.prototype.fetchForm = function (e) {
        e.preventDefault();
        var model = new LoginModel();
        model.Login = $('#login').val();
        model.Password = $('#password').val();
        model.Path = $('#Path').val();
        $.ajax({
            url: Global.url + '/base/start/login',
            method: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(model),
            success: function (read) {
                if (read.Redirect) {
                    document.location.href = Global.url + read.Path;
                }
                else {
                    $('#loginMessage').text(read.Message).removeClass('hidden');
                }
            }
        });
    };
    return LoginContent;
}(React.Component));
export { LoginContent };
var LoginModel = /** @class */ (function () {
    function LoginModel() {
    }
    return LoginModel;
}());
//# sourceMappingURL=LoginContent.js.map