var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as $ from 'jquery';
import { Button, ButtonStyle } from "../../../Components/Button/Button";
import { TextBox } from "../../../Components/TextBox/TextBox";
import { Link } from "../../../Components/Link/Link";
var FormLogin = /** @class */ (function (_super) {
    __extends(FormLogin, _super);
    function FormLogin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormLogin.prototype.render = function () {
        return (React.createElement("form", null,
            this.props.Message ?
                React.createElement("div", { id: 'loginMessage', className: "alert alert-danger" }, this.props.Message)
                : null,
            React.createElement(TextBox, { id: 'login', placeholder: 'Email или телефон', value: this.props.Login, label: 'Email или телефон', onFocus: this.props.onLoginFocus, onKeyPress: this.props.onLoginPress }),
            React.createElement(TextBox, { id: 'password', placeholder: 'Пароль', label: 'Пароль', password: true, value: this.props.Password, onFocus: this.props.onPasswordFocus, onKeyPress: this.props.onPasswordPress }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Войти', onClick: this.props.onSubmit }),
            React.createElement("div", { className: "text-center" },
                React.createElement(Link, { caption: 'Забыли пароль?' }))));
    };
    FormLogin.prototype.componentDidMount = function () {
        $('[name="login"]').focus();
    };
    return FormLogin;
}(React.Component));
export { FormLogin };
var LoginModel = /** @class */ (function () {
    function LoginModel() {
    }
    return LoginModel;
}());
//# sourceMappingURL=FormLogin.js.map