var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as $ from 'jquery';
import * as React from 'react';
import { Button, ButtonStyle } from "../../../Components/Button/Button";
import { TextBox } from "../../../Components/TextBox/TextBox";
var FormRegMe = /** @class */ (function (_super) {
    __extends(FormRegMe, _super);
    function FormRegMe() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormRegMe.prototype.render = function () {
        return (React.createElement("form", null,
            React.createElement("div", { className: "alert alert-danger hidden" }, this.message),
            React.createElement(TextBox, { id: 'mail', placeholder: 'Email', label: 'Email' }),
            React.createElement(TextBox, { id: 'phone', placeholder: 'Телефон', label: 'Телефон' }),
            React.createElement(TextBox, { id: 'newpass', placeholder: 'Пароль', password: true, label: 'Пароль' }),
            React.createElement(TextBox, { id: 'repass', placeholder: 'Повторите пароль', password: true, label: 'Повторите пароль' }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Зарегистрироваться' })));
    };
    FormRegMe.prototype.componentDidMount = function () {
        $('[name="mail"]').focus();
    };
    return FormRegMe;
}(React.Component));
export { FormRegMe };
var RegMeModel = /** @class */ (function () {
    function RegMeModel() {
    }
    return RegMeModel;
}());
//# sourceMappingURL=FormRegMe.js.map