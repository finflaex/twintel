var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as $ from 'jquery';
import * as React from 'react';
import { Button, ButtonStyle } from "../../../Components/Button/Button";
import { Global } from "../../../Global";
import { TextBox } from "../../../Components/TextBox/TextBox";
var RegContent = /** @class */ (function (_super) {
    __extends(RegContent, _super);
    function RegContent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RegContent.prototype.render = function () {
        return (React.createElement("form", null,
            React.createElement("div", { className: "alert alert-danger hidden" }, this.message),
            React.createElement(TextBox, { id: 'mail', placeholder: 'Email' }),
            React.createElement(TextBox, { id: 'phone', placeholder: 'Телефон' }),
            React.createElement(TextBox, { id: 'newpass', placeholder: 'Пароль', password: true }),
            React.createElement(TextBox, { id: 'repass', placeholder: 'Повторите пароль', password: true }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Зарегистрироваться', onClick: this.fetchForm })));
    };
    RegContent.prototype.componentDidMount = function () {
        $('[name="login"]').focus();
    };
    RegContent.prototype.fetchForm = function (e) {
        e.preventDefault();
        var model = new RegMeModel();
        model.Mail = $('#mail').val();
        model.Phone = $('#phone').val();
        model.Password = $('#newpass').val();
        model.Repass = $('#repass').val();
        model.Path = $('#Path').val();
        $.ajax({
            url: Global.url + '/base/start/login',
            method: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(model),
            success: function (read) {
                if (read.Redirect) {
                    document.location.href = Global.url + read.Path;
                }
                else {
                    $('#regMeMessage').text(read.Message).removeClass('hidden');
                }
            }
        });
    };
    return RegContent;
}(React.Component));
export { RegContent };
var RegMeModel = /** @class */ (function () {
    function RegMeModel() {
    }
    return RegMeModel;
}());
//# sourceMappingURL=RegContent.js.map