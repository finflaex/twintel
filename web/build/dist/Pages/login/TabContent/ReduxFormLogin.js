var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../Components/TextBox/TextBox';
import { Button, ButtonStyle } from '../../../Components/Button/Button';
//const renderField = (data:any) => {
//    return (
//        <div>
//            <TextBox formInput={data.input}/>
//            {/*<label>{data.label}</label>*/}
//            {/*<div>*/}
//            {/*<input */}
//            {/*{...data.input} */}
//            {/*placeholder={data.label} */}
//            {/*type={data.type}*/}
//            {/*/>*/}
//            {/*</div>*/}
//        </div>
//    ) 
//};
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.renderField = function (_a) {
            var input = _a.input, label = _a.label, type = _a.type;
            return (React.createElement("div", null,
                React.createElement(TextBox, { formInput: input, label: label, placeholder: label, type: type })));
        };
        return _this;
    }
    Form.prototype.render = function () {
        var _a = this.props, handleSubmit = _a.handleSubmit, reset = _a.reset;
        var submit = function (values) { return console.log(values); };
        return (React.createElement("form", { onSubmit: handleSubmit(submit) },
            React.createElement(Field, { name: "login", component: this.renderField, label: "Email или телефон" }),
            React.createElement(Field, { name: "password", component: this.renderField, label: "Пароль", type: "password" }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Войти' })));
    };
    return Form;
}(React.Component));
var ReduxFormLogin = reduxForm({ form: 'post' })(Form);
export { ReduxFormLogin };
//# sourceMappingURL=ReduxFormLogin.js.map