// step 5
import * as actions from './actions';
import { connect } from 'react-redux';
import { FormLogin } from "../TabContent/FormLogin";
export function mapStateToProps(_a) {
    var Login = _a.Login, Password = _a.Password;
    return {
        Login: Login,
        Password: Password,
    };
}
export function mapDispatchToProps(dispatch) {
    return {
        onSubmit: function (e) {
            e.preventDefault();
            dispatch(actions.actionFormLoginSubmit());
            return false;
        }
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
//# sourceMappingURL=container.js.map