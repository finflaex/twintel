// step 4
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { LOGIN_SUBMIT } from "./constants";
export function reducerFormLogin(state, action) {
    switch (action.type) {
        case LOGIN_SUBMIT:
            return __assign({}, state);
        default:
            return state;
    }
}
//# sourceMappingURL=reducer.js.map