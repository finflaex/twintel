var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import 'jquery';
import 'bootstrap';
import './FormLogin.scss';
import { TabSheet } from '../../../Components/TabSheet/TabSheet';
import { TabItem } from '../../../Components/TabItem/TabItem';
import { Logo } from '../../../Components/Logo/Logo';
import { FormRegMe } from "../TabContent/FormRegMe";
import { FormLogin } from "../TabContent/FormLogin";
var TabsFormLogin = /** @class */ (function (_super) {
    __extends(TabsFormLogin, _super);
    function TabsFormLogin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TabsFormLogin.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("div", { className: "text-center" },
                React.createElement(Logo, null),
                React.createElement("h3", null, "\u041B\u0438\u0447\u043D\u044B\u0439 \u043A\u0430\u0431\u0438\u043D\u0435\u0442")),
            React.createElement("div", { className: "formLogin" },
                React.createElement(TabSheet, { myChildren: [
                        new TabItem('login', 'Вход', React.createElement(FormLogin, null), true),
                        new TabItem('registration', 'Регистрация', React.createElement(FormRegMe, null))
                    ] }))));
    };
    return TabsFormLogin;
}(React.Component));
export default TabsFormLogin;
//# sourceMappingURL=TabsFormLogin.js.map