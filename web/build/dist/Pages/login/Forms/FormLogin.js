var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import '../../../Styles.scss';
//import '@progress/kendo-ui';
//import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js';
//kendo.culture('ru-RU');
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../Components/TextBox/TextBox';
import { Button, ButtonStyle } from '../../../Components/Button/Button';
//import { DropDownList } from '@progress/kendo-dropdowns-react-wrapper';
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.renderField = function (_a) {
            var input = _a.input, label = _a.label, type = _a.type;
            return (React.createElement(TextBox, { formInput: input, label: label, placeholder: label, password: type == 'password' }));
        };
        return _this;
    }
    //renderSelect(data: any) { 
    //    const options = {
    //        name: data.name,
    //        id: data.name,
    //        filter: "startswith",
    //        dataTextField: "ProductName",
    //        dataValueField: "ProductID",
    //        dataSource: {
    //            type: "odata",
    //            serverFiltering: true,
    //            transport: {
    //                read: {
    //                    url: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Products",
    //                }
    //            }
    //        },
    //        select: data.input.onFocus,
    //        change: data.input.onChange,
    //        //close: onClose,
    //        //open: onOpen,
    //        //filtering: onFiltering,
    //        //dataBound: onDataBound
    //    }
    //    console.log(data);
    //    return (
    //    <div>
    //        <label>Компания</label>
    //        <br />
    //        <DropDownList
    //                {...options}
    //        />
    //    </div>
    //);
    //}
    Form.prototype.render = function () {
        var _a = this.props, handleSubmit = _a.handleSubmit, reset = _a.reset;
        var submit = function (values) { return console.log(values); };
        return (React.createElement("form", { onSubmit: handleSubmit(submit) },
            React.createElement(Field, { name: "login", component: this.renderField, label: "Email или телефон", type: 'text' }),
            React.createElement(Field, { name: "password", component: this.renderField, label: "Пароль", type: "password" }),
            React.createElement("div", { className: "dropDownList" },
                React.createElement("label", { htmlFor: "company" }, "\u041A\u043E\u043C\u043F\u0430\u043D\u0438\u044F"),
                React.createElement("div", null,
                    React.createElement(Field, { name: "company", component: "select", id: "company", className: "k-dropdown" },
                        React.createElement("option", { value: "company1" }, "\u041E\u041E\u041E \"\u0420\u043E\u043C\u0430\u0448\u043A\u0430\""),
                        React.createElement("option", { value: "company2" }, "\u041E\u041E\u041E \"\u0412\u0430\u0441\u0438\u043B\u0451\u043A\""),
                        React.createElement("option", { value: "company3" }, "\u041E\u041E\u041E \"\u0420\u043E\u0433\u0430\""),
                        React.createElement("option", { value: "company4" }, "\u041E\u041E\u041E \"\u041A\u043E\u043F\u044B\u0442\u0430\"")))),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Войти', type: 'submit' })));
    };
    return Form;
}(React.Component));
var ReduxFormLogin = reduxForm({ form: 'login' })(Form);
export { ReduxFormLogin };
//# sourceMappingURL=FormLogin.js.map