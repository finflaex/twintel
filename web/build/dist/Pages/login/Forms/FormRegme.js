var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../Components/TextBox/TextBox';
import { Button, ButtonStyle } from '../../../Components/Button/Button';
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.renderField = function (_a) {
            var input = _a.input, label = _a.label, type = _a.type;
            return (React.createElement(TextBox, { formInput: input, label: label, placeholder: label, type: type }));
        };
        return _this;
    }
    Form.prototype.render = function () {
        //const {handleSubmit, reset} = this.props;
        { }
        // const submit = (values: any) => console.log(values);
        return (React.createElement("form", null,
            React.createElement(Field, { name: "email", component: this.renderField, label: "Email", type: 'email' }),
            React.createElement(Field, { name: "phone", component: this.renderField, label: "Телефон", type: "tel" }),
            React.createElement(Field, { name: "password1", component: this.renderField, label: "Пароль", type: "password" }),
            React.createElement(Field, { name: "password2", component: this.renderField, label: "Повторите пароль", type: "password" }),
            React.createElement(Button, { style: ButtonStyle.primary, caption: 'Зарегистрироваться' })));
    };
    return Form;
}(React.Component));
var ReduxFormRegMe = reduxForm({ form: 'regme' })(Form);
export { ReduxFormRegMe };
//# sourceMappingURL=FormRegme.js.map