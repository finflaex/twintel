import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import './login.scss';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
import '../../kendo.custom.css';
import { connectHub } from "../../Types/Hubs";
import { TabItem } from "../../Components/TabItem/TabItem";
import { TabSheet } from "../../Components/TabSheet/TabSheet";
import { Logo } from "../../Components/Logo/Logo";
import { FormLoginProvider, FormRegMeProvider } from "./provider";
ReactDOM.render(React.createElement("div", { id: "form_authorization" },
    React.createElement("div", { className: "text-center" },
        React.createElement(Logo, null),
        React.createElement("h3", null, "\u041B\u0438\u0447\u043D\u044B\u0439 \u043A\u0430\u0431\u0438\u043D\u0435\u0442")),
    React.createElement("div", { className: "formLogin" },
        React.createElement(TabSheet, { myChildren: [
                new TabItem('login', 'Вход', React.createElement(FormLoginProvider, null), true),
                new TabItem('registration', 'Регистрация', React.createElement(FormRegMeProvider, null))
            ] }))), document.getElementById('root'));
connectHub.start();
//# sourceMappingURL=login.js.map