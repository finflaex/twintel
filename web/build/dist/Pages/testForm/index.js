import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
var reducers = {
    form: formReducer
};
function logger(_a) {
    var getState = _a.getState;
    return function (next) { return function (action) {
        var returnValue = next(action);
        var model = getState();
        console.log('will dispatch', action);
        console.log('state after dispatch', getState());
        var data = {
            type: action.type.split('\/')[1],
            name: action.meta.field,
            form: action.meta.form,
            value: action.payload
        };
        if (data.name) {
            //console.log(getState().form.post.values);
            console.log(data);
        }
        return returnValue;
    }; };
}
var reducer = combineReducers(reducers);
var store = createStore(reducer, applyMiddleware(logger));
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ReduxForm } from "./ReduxForm";
import { Provider } from "react-redux";
ReactDOM.render(React.createElement(Provider, { store: store },
    React.createElement(ReduxForm, null)), document.getElementById('root'));
//# sourceMappingURL=index.js.map