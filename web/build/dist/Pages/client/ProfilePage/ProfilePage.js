var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import '@progress/kendo-ui';
import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js';
kendo.culture('ru-RU');
import { Button, ButtonStyle } from '../../../Components/Button/Button';
import { DTextBox } from '../../../Components/DTextBox/DTextBox';
import './ProfilePage.scss';
import { TextBox } from "../../../Components/TextBox/TextBox";
import { Gender } from '../../../Components/RadioGroup/Gender/Gender';
var ProfilePage = /** @class */ (function (_super) {
    __extends(ProfilePage, _super);
    function ProfilePage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ProfilePage.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("h1", null, "\u0417\u0430\u043F\u043E\u043B\u043D\u0438\u0442\u044C \u043F\u0440\u043E\u0444\u0438\u043B\u044C"),
            React.createElement("p", null, "\u0414\u043E\u0431\u0440\u043E \u043F\u043E\u0436\u0430\u043B\u043E\u0432\u0430\u0442\u044C \u0432 \u043B\u0438\u0447\u043D\u044B\u0439 \u043A\u0430\u0431\u0438\u043D\u0435\u0442 TWINTEL. \u0417\u0430\u043F\u043E\u043B\u043D\u0438\u0442\u0435 \u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u0434\u0430\u043D\u043D\u044B\u0435 \u043E \u0441\u0435\u0431\u0435."),
            React.createElement("div", { className: "panel" },
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement(TextBox, { placeholder: 'Фамилия', label: 'Фамилия' })),
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement(TextBox, { placeholder: 'Имя', label: 'Имя' })),
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement(TextBox, { placeholder: 'Отчество', label: 'Отчество' }))),
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement("label", null, "\u0414\u0435\u043D\u044C \u0440\u043E\u0436\u0434\u0435\u043D\u0438\u044F")),
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement("label", null, "\u041F\u043E\u043B"),
                        React.createElement(Gender, null))),
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement("label", null, "\u041F\u043E\u0447\u0442\u0430"),
                        React.createElement(DTextBox, { placeholder: 'Почта', caption: '+ Добавить почту', num: 1 })),
                    React.createElement("div", { className: "col-md-4" },
                        React.createElement("label", null, "\u0422\u0435\u043B\u0435\u0444\u043E\u043D"),
                        React.createElement(DTextBox, { placeholder: 'Телефон', caption: '+ Добавить телефон' }))),
                React.createElement("div", { className: "row" },
                    React.createElement("div", { className: "col-xs-12" },
                        React.createElement(Button, { style: ButtonStyle.primary, caption: 'Сохранить' }))))));
    };
    return ProfilePage;
}(React.Component));
// todo создать метод для отправки данных на сервер
export { ProfilePage };
//# sourceMappingURL=ProfilePage.js.map