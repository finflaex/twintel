import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './client.scss';
// import '@progress/kendo-ui';
// import { Button } from '@progress/kendo-buttons-react-wrapper';
import '@progress/kendo-ui/css/web/kendo.common-material.css';
// import '@progress/kendo-ui/css/web/kendo.material.css';
import '../../kendo.custom.css';
import { Header } from '../../Components/Header/Header';
import { Footer } from '../../Components/Footer/Footer';
import { MenuActions } from '../../Components/MenuActions/MenuActions';
import { ProfilePage } from './ProfilePage/ProfilePage';
import { MenuProjects } from '../../Components/MenuProjects/MenuProjects';
ReactDOM.render(React.createElement("div", null,
    React.createElement(Header, null),
    React.createElement("div", { className: "flexbox-row" },
        React.createElement("div", { className: "flexbox-col list" },
            React.createElement(MenuProjects, null)),
        React.createElement("div", { className: "flexbox-col list" },
            React.createElement(MenuActions, null)),
        React.createElement("div", { className: "flexbow-col content" },
            React.createElement(ProfilePage, null))),
    React.createElement(Footer, null)), document.getElementById('root'));
// todo kendo test
// import * as $ from 'jquery';
// import '@progress/kendo-ui';
// import '@progress/kendo-ui/js/cultures/kendo.culture.ru-RU.js'
// kendo.culture('ru-RU');
// $('#kendo_test').kendoDropDownList(); 
//# sourceMappingURL=client.js.map