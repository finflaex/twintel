import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Hello from '../../testRedux/containers/Hello';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { enthusiasm } from '../../testRedux/reducers/index';
var store = createStore(enthusiasm, {
    enthusiasmLevel: 1,
    languageName: 'world',
});
ReactDOM.render(React.createElement("div", null,
    React.createElement(Provider, { store: store },
        React.createElement(Hello, null))), document.getElementById('root'));
//# sourceMappingURL=index.js.map