// export default class Cookie {
//     static getCookie(name: string): string {
//         const nameLenPlus = (name.length + 1);
//         return document.cookie
//             .split(';')
//             .map(c => c.trim())
//             .filter(cookie => cookie.substring(0, nameLenPlus) === `${name}=`)
//             .map(cookie =>  decodeURIComponent(cookie.substring(nameLenPlus)))
//             [0] || '';
//     }
// }
var cookie = /** @class */ (function () {
    function cookie() {
    }
    cookie.read = function (name) {
        var result = new RegExp('(?:^|; )' + encodeURIComponent(name) + '=([^;]*)').exec(document.cookie);
        return result ? result[1] : null;
    };
    cookie.write = function (name, value, minuts) {
        if (!minuts) {
            minuts = 20;
        }
        var date = new Date();
        date.setTime(date.getTime() + (minuts * 60 * 1000));
        var expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + value + expires + "; path=/";
    };
    cookie.remove = function (name) {
        this.write(name, "", -1);
    };
    return cookie;
}());
export { cookie };
//# sourceMappingURL=Cookie.js.map