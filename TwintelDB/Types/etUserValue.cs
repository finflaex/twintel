﻿namespace TwintelDB.Types
{
    public enum etUserValue
    {
        unknown = 0,
        session = 1,
        caption = 2,
        role = 3,
        login = 4,
        password = 5,
        connect_type = 6
    }
}