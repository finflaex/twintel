﻿using System;
using System.Collections.Generic;
using System.Text;
using TwintelDB.Abstracts;
using TwintelDB.Interfaces;

namespace TwintelDB.Tables
{
    public class ctUser
        : btObject<ctUserValue>
    {
        public ctUser()
        {
            Persons = new HashSet<ctPersonUser>();
        }

        public virtual ICollection<ctPersonUser> Persons { get; set; }
    }
}