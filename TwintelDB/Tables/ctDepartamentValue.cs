﻿using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctDepartamentValue
        :btDynamic<ctDepartament, ctDepartamentValue, etDepartamentValue, ctDepartamentStatic, etDepartamentStatic>
    {
    }
}