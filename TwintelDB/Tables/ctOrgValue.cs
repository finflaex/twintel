﻿using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctOrgValue
        : btDynamic<ctOrg, ctOrgValue, etOrgValue, ctOrgStatic, etOrgStatic>
    {
    }
}