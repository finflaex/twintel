﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctOrgDepartament
        : btLink<ctOrg, ctDepartament, etOrgDepartament>
    {
        public static ModelBuilder Build(ModelBuilder db)
        {
            db.Entity<ctOrgDepartament>()
                .HasOne(v => v.First).WithMany(v => v.Departaments)
                .HasForeignKey(v => v.FirstGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<ctOrgDepartament>()
                .HasOne(v => v.Second).WithMany(v => v.Orgs)
                .HasForeignKey(v => v.SecondGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            return db;
        }
    }
}
