﻿using System.Collections.Generic;
using TwintelDB.Abstracts;

namespace TwintelDB.Tables
{
    public class ctHolding
        : btObject<ctHoldingValue>
    {
        public ctHolding()
        {
            Orgs = new HashSet<ctHoldingOrg>();
        }
        public virtual ICollection<ctHoldingOrg> Orgs { get; set; }
    }
}