﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace TwintelDB.Tables
{
    public class ctBankDetails
        : fxaGuidTable
    {
        public Guid OrgGuid { get; set; }
        public virtual ctOrg Org { get; set; }

        public static ModelBuilder Build(ModelBuilder db)
        {
            db.Entity<ctBankDetails>()
                .HasOne(v => v.Org).WithMany(v => v.BankDetails)
                .HasForeignKey(v => v.OrgGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            return db;
        }
    }
}