﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Abstracts;
using FinflaexExtension.DataBase.Table.Interfaces;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctHoldingOrg
        : btLink<ctHolding, ctOrg, etHoldingOrg>
    {
        public static void Build(ModelBuilder db)
        {
            db.Entity<ctHoldingOrg>()
                .HasOne(v => v.First).WithMany(v => v.Orgs)
                .HasForeignKey(v => v.FirstGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<ctHoldingOrg>()
                .HasOne(v => v.Second).WithMany(v => v.Holdings)
                .HasForeignKey(v => v.SecondGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
