﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctDepartamentPerson
        : btLink<ctDepartament, ctPerson, etDepartamentPerson>
    {
        public static ModelBuilder Build(ModelBuilder db)
        {
            db.Entity<ctDepartamentPerson>()
                .HasOne(v => v.First).WithMany(v => v.Persons)
                .HasForeignKey(v => v.FirstGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<ctDepartamentPerson>()
                .HasOne(v => v.Second).WithMany(v => v.Departaments)
                .HasForeignKey(v => v.SecondGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            return db;
        }
    }
}
