﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctPersonUser
        : btLink<ctPerson, ctUser, etPersonUser>
    {
        public static ModelBuilder Build(ModelBuilder db)
        {
            db.Entity<ctPersonUser>()
                .HasOne(v => v.First).WithMany(v => v.Users)
                .HasForeignKey(v => v.FirstGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<ctPersonUser>()
                .HasOne(v => v.Second).WithMany(v => v.Persons)
                .HasForeignKey(v => v.SecondGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            return db;
        }
    }
}
