﻿using TwintelDB.Abstracts;
using TwintelDB.Types;

namespace TwintelDB.Tables
{
    public class ctHoldingStatic
        : btStatic<ctHoldingValue, etHoldingStatic>
    {
    }
}