﻿using System;
using System.Collections.Generic;
using System.Text;
using TwintelDB.Abstracts;
using TwintelDB.Interfaces;

namespace TwintelDB.Tables
{
    public class ctDepartament
        : btObject<ctDepartamentValue>
    {
        public ctDepartament()
        {
            Orgs = new HashSet<ctOrgDepartament>();
            Persons = new HashSet<ctDepartamentPerson>();
        }
        public virtual ICollection<ctOrgDepartament> Orgs { get; set; }
        public virtual ICollection<ctDepartamentPerson> Persons { get; set; }
    }
}
