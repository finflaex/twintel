﻿using System.Collections.Generic;
using TwintelDB.Abstracts;

namespace TwintelDB.Tables
{
    public class ctPerson
        : btObject<ctPersonValue>
    {
        public ctPerson()
        {
            Departaments = new HashSet<ctDepartamentPerson>();
            Users = new HashSet<ctPersonUser>();
        }
        
        public virtual ICollection<ctDepartamentPerson> Departaments { get; set; }
        public virtual ICollection<ctPersonUser> Users { get; set; }
    }
}