﻿using System.Collections.Generic;
using TwintelDB.Abstracts;

namespace TwintelDB.Tables
{
    public class ctOrg
        : btObject<ctOrgValue>
    {
        public ctOrg()
        {
            Holdings = new HashSet<ctHoldingOrg>();
            Departaments = new HashSet<ctOrgDepartament>();
            BankDetails = new HashSet<ctBankDetails>();
        }
        public virtual ICollection<ctHoldingOrg> Holdings { get; set; }
        public virtual ICollection<ctOrgDepartament> Departaments { get; set; }
        public virtual ICollection<ctBankDetails> BankDetails { get; set; }
    }
}