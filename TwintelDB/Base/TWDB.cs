﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Base;
using Microsoft.EntityFrameworkCore;

namespace TwintelDB.Base
{
    public partial class TWDB : fxaDbContext<TWDB>
    {
        protected override void OnConfiguring(DbContextOptionsBuilder db)
        {
            db.UseSqlServer(ConnectTo(Host.local));
            //base.OnConfiguring(db);
        }
    }
}