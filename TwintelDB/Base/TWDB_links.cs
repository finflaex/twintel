﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Tables;

namespace TwintelDB.Base
{
    partial class TWDB
    {
        protected override void OnModelCreating(ModelBuilder db)
        {
            base.OnModelCreating(db);

            //-- objects

            ctHoldingValue.Build(db);
            ctOrgValue.Build(db);
            ctDepartamentValue.Build(db);
            ctPostValue.Build(db);
            ctPersonValue.Build(db);
            ctUserValue.Build(db);

            //-- link

            ctHoldingOrg.Build(db);
            ctOrgDepartament.Build(db);
            ctDepartamentPerson.Build(db);
            ctPersonUser.Build(db);

            //-- other

            ctBankDetails.Build(db);
        }
    }
}