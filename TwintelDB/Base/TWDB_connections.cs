﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace TwintelDB.Base
{
    public enum Host
    {
        local = 0,
        test = 1,
        @public = 2
    }

    partial class TWDB
    {
        private static string ConnectTo(Host host)
        {
            switch (host)
            {
                default: return Localhost.ToString();
                case Host.test: return Testlhost.ToString();
                case Host.@public: return Publiclhost.ToString();
            }
        }

        private static SqlConnectionStringBuilder Localhost => new SqlConnectionStringBuilder
        {
            DataSource = @"(localdb)\MSSQLLocalDB",
            InitialCatalog = $"{nameof(TWDB)}",
            IntegratedSecurity = true,
            ConnectTimeout = 30,
            Encrypt = false,
            TrustServerCertificate = true,
            ApplicationIntent = ApplicationIntent.ReadWrite,
            MultiSubnetFailover = false
        };

        private static SqlConnectionStringBuilder Testlhost => new SqlConnectionStringBuilder
        {
            DataSource = "192.168.10.11",
            UserID = "sa",
            Password = "Sa123456",

            InitialCatalog = $"{nameof(TWDB)}",
            IntegratedSecurity = false,
            ConnectTimeout = 30,
            Encrypt = false,
            TrustServerCertificate = true,
            ApplicationIntent = ApplicationIntent.ReadWrite,
            MultiSubnetFailover = false
        };

        private static SqlConnectionStringBuilder Publiclhost => new SqlConnectionStringBuilder
        {
            DataSource = "192.168.10.11",
            UserID = "sa",
            Password = "Sa123456",

            InitialCatalog = $"{nameof(TWDB)}",
            IntegratedSecurity = false,
            ConnectTimeout = 30,
            Encrypt = false,
            TrustServerCertificate = true,
            ApplicationIntent = ApplicationIntent.ReadWrite,
            MultiSubnetFailover = false
        };
    }
}
