﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Tables;

namespace TwintelDB.Base
{
    partial class TWDB
    {
        public DbSet<ctHolding> OrgGroups { get; set; }
        public DbSet<ctHoldingValue> OrgGroupValues { get; set; }
        public DbSet<ctHoldingStatic> OrgGroupStatics { get; set; }

        public DbSet<ctOrg> Orgs { get; set; }
        public DbSet<ctOrgValue> OrgValues { get; set; }
        public DbSet<ctOrgStatic> OrgStatics { get; set; }

        public DbSet<ctDepartament> Departaments { get; set; }
        public DbSet<ctDepartamentValue> DepartamentValues { get; set; }
        public DbSet<ctDepartamentStatic> DepartamentStatics { get; set; }

        public DbSet<ctPost> Posts { get; set; }
        public DbSet<ctPostValue> PostValues { get; set; }
        public DbSet<ctPostStatic> PostStatics { get; set; }

        public DbSet<ctPerson> Persons { get; set; }
        public DbSet<ctPersonValue> PersonValues { get; set; }
        public DbSet<ctPersonStatic> PersonStatics { get; set; }

        public DbSet<ctUser> Users { get; set; }
        public DbSet<ctUserValue> UserValues { get; set; }
        public DbSet<ctUserStatic> UserStatics { get; set; }

        //------ links -----

        public DbSet<ctHoldingOrg> HoldingOrgs { get; set; }
        public DbSet<ctOrgDepartament> OrgDepartaments { get; set; }
        public DbSet<ctDepartamentPerson> DepartamentPersons { get; set; }
        public DbSet<ctPersonUser> PersonUsers { get; set; }
    }
}