﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using FinflaexExtension.DataBase.Table.Abstracts;
using FinflaexExtension.DataBase.Table.Interfaces;
using TwintelDB.Interfaces;

namespace TwintelDB.Abstracts
{
    public class btStatic<TDynamic, TType>
        : fxaGuidTable
            , fitStatic
            , fxiTypeTable<TType>
        where TDynamic : class, fitDynamic
        where TType : struct
    {
        public btStatic()
        {
            Dynamics = new HashSet<TDynamic>();
        }

        public virtual ICollection<TDynamic> Dynamics { get; set; }
        public string Key { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }

        public TType Type { get; set; }
    }
}