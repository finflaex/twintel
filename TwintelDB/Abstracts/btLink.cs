﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Abstracts;
using FinflaexExtension.DataBase.Table.Interfaces;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Interfaces;

namespace TwintelDB.Abstracts
{
    public class btLink<TFirst,TSecond, TType>
        : fxaGuidTable
        , fitLink
        , fxiTypeTable<TType>
        where TFirst: class, fitObject
        where TSecond: class, fitObject
        where TType: struct 
    {
        public DateTimeOffset CreateDate { get; set; }
        public Guid? CreateAuthor { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public Guid? UpdateAuthor { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }

        public Guid FirstGuid { get; set; }
        public virtual TFirst First { get; set; }

        public Guid SecondGuid { get; set; }
        public virtual TSecond Second { get; set; }

        public TType Type { get; set; }
    }
}
