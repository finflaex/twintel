﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using FinflaexExtension.DataBase.Table.Abstracts;
using TwintelDB.Interfaces;

namespace TwintelDB.Abstracts
{
    public class btObject<TDynamic>
        : fxaGuidTable
            , fitObject
        where TDynamic : fitDynamic
    {
        public btObject()
        {
            Dynamics = new HashSet<TDynamic>();
        }

        public virtual ICollection<TDynamic> Dynamics { get; set; }
    }
}