﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using FinflaexExtension.DataBase.Table.Abstracts;
using FinflaexExtension.DataBase.Table.Interfaces;
using Microsoft.EntityFrameworkCore;
using TwintelDB.Interfaces;

namespace TwintelDB.Abstracts
{
    public class btDynamic<TObject, TThis, TType, TStatic, TStaticType>
        : fxaGuidTable
            , fitDynamic
            , fxiTypeTable<TType>
        where TThis : btDynamic<TObject, TThis, TType, TStatic, TStaticType>
        where TObject : btObject<TThis>, fitObject
        where TStatic : btStatic<TThis, TStaticType>, fitStatic
        where TType : struct
        where TStaticType : struct
    {
        public Guid ObjectGuid { get; set; }
        public virtual TObject Object { get; set; }

        public Guid? StaticGuid { get; set; }
        public virtual TStatic Static { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public Guid? CreateAuthor { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public Guid? UpdateAuthor { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public string StringValue { get; set; }
        public bool BoolValue { get; set; }
        public DateTimeOffset DateTimeOffsetValue { get; set; }
        public decimal DecimalValue { get; set; }
        public Guid GuidValue { get; set; }
        public int Order { get; set; }
        public byte[] RawValue { get; set; }
        public int IntValue { get; set; }

        public string Key { get; set; }

        public TType Type { get; set; }

        public static void Build(ModelBuilder db)
        {
            db.Entity<TThis>()
                .HasOne(v => v.Object).WithMany(v => v.Dynamics)
                .HasForeignKey(v => v.ObjectGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<TThis>()
                .HasOne(v => v.Static).WithMany(v => v.Dynamics)
                .HasForeignKey(v => v.StaticGuid).IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}