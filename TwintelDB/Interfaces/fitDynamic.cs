﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Interfaces;

namespace TwintelDB.Interfaces
{
    public interface fitDynamic
        : fxiGuidTable
            , fxiCreateTable
            , fxiUpdateTable
            , fxiStartEndTable
            , fxiKeyTable
            , fxiStringValueTable
            , fxiBoolValueTable
            , fxiDateTimeOffsetValueTable
            , fxiDecimalValueTable
            , fxiIntValueTable
            , fxiGuidValueTable
            , fxiOrderTable
            , fxiRawValueTable
    {
    }
}