﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Interfaces;

namespace TwintelDB.Interfaces
{
    public interface fitStatic
        : fxiGuidTable
            , fxiKeyTable
            , fxiCaptionTable
            , fxiDescriptionTable
    {
    }
}