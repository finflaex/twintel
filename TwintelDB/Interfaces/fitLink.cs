﻿using System;
using System.Collections.Generic;
using System.Text;
using FinflaexExtension.DataBase.Table.Interfaces;

namespace TwintelDB.Interfaces
{
    interface fitLink
        : fxiGuidTable
            , fxiCreateTable
            , fxiUpdateTable
            , fxiStartEndTable
    {
    }
}
