﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TwintelDB.Migrations
{
    public partial class bank_details : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ctBankDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OrgGuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ctBankDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ctBankDetails_Orgs_OrgGuid",
                        column: x => x.OrgGuid,
                        principalTable: "Orgs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ctBankDetails_OrgGuid",
                table: "ctBankDetails",
                column: "OrgGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ctBankDetails");
        }
    }
}
