﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using TwintelDB.Base;
using TwintelDB.Tables;
using TwintelDB.Types;

namespace TwintelDB.Migrations
{
    [DbContext(typeof(TWDB))]
    [Migration("20180121131258_start")]
    partial class start
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TwintelDB.Tables.ctDepartament", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Departaments");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctDepartamentPerson", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("FirstGuid");

                    b.Property<Guid>("SecondGuid");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("FirstGuid");

                    b.HasIndex("SecondGuid");

                    b.ToTable("DepartamentPersons");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctDepartamentStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("DepartamentStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctDepartamentValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("DepartamentValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHolding", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("OrgGroups");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHoldingOrg", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("FirstGuid");

                    b.Property<Guid>("SecondGuid");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("FirstGuid");

                    b.HasIndex("SecondGuid");

                    b.ToTable("HoldingOrgs");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHoldingStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("OrgGroupStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHoldingValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("OrgGroupValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrg", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Orgs");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrgDepartament", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("FirstGuid");

                    b.Property<Guid>("SecondGuid");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("FirstGuid");

                    b.HasIndex("SecondGuid");

                    b.ToTable("OrgDepartaments");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrgStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("OrgStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrgValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("OrgValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPerson", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Persons");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPersonStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("PersonStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPersonUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("FirstGuid");

                    b.Property<Guid>("SecondGuid");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("FirstGuid");

                    b.HasIndex("SecondGuid");

                    b.ToTable("PersonUsers");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPersonValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("PersonValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPost", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPostStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("PostStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPostValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("PostValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctUserStatic", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Caption");

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("UserStatics");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctUserValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<Guid?>("CreateAuthor");

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<DateTimeOffset>("DateTimeOffsetValue");

                    b.Property<decimal>("DecimalValue");

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("GuidValue");

                    b.Property<int>("IntValue");

                    b.Property<string>("Key");

                    b.Property<Guid>("ObjectGuid");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("StaticGuid");

                    b.Property<string>("StringValue");

                    b.Property<int>("Type");

                    b.Property<Guid?>("UpdateAuthor");

                    b.Property<DateTimeOffset>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("ObjectGuid");

                    b.HasIndex("StaticGuid");

                    b.ToTable("UserValues");
                });

            modelBuilder.Entity("TwintelDB.Tables.ctDepartamentPerson", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctDepartament", "First")
                        .WithMany("Persons")
                        .HasForeignKey("FirstGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctPerson", "Second")
                        .WithMany("Departaments")
                        .HasForeignKey("SecondGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctDepartamentValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctDepartament", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctDepartamentStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHoldingOrg", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctHolding", "First")
                        .WithMany("Orgs")
                        .HasForeignKey("FirstGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctOrg", "Second")
                        .WithMany("Holdings")
                        .HasForeignKey("SecondGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctHoldingValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctHolding", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctHoldingStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrgDepartament", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctOrg", "First")
                        .WithMany("Departaments")
                        .HasForeignKey("FirstGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctDepartament", "Second")
                        .WithMany("Orgs")
                        .HasForeignKey("SecondGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctOrgValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctOrg", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctOrgStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPersonUser", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctPerson", "First")
                        .WithMany("Users")
                        .HasForeignKey("FirstGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctUser", "Second")
                        .WithMany("Persons")
                        .HasForeignKey("SecondGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPersonValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctPerson", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctPersonStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctPostValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctPost", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctPostStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TwintelDB.Tables.ctUserValue", b =>
                {
                    b.HasOne("TwintelDB.Tables.ctUser", "Object")
                        .WithMany("Dynamics")
                        .HasForeignKey("ObjectGuid")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TwintelDB.Tables.ctUserStatic", "Static")
                        .WithMany("Dynamics")
                        .HasForeignKey("StaticGuid")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
