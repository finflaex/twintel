﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TwintelDB.Migrations
{
    public partial class start : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departaments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departaments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DepartamentStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartamentStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrgGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrgGroupStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgGroupStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orgs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orgs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrgStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserStatics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserStatics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DepartamentValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartamentValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartamentValues_Departaments_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "Departaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepartamentValues_DepartamentStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "DepartamentStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrgGroupValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgGroupValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgGroupValues_OrgGroups_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "OrgGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrgGroupValues_OrgGroupStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "OrgGroupStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HoldingOrgs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    FirstGuid = table.Column<Guid>(nullable: false),
                    SecondGuid = table.Column<Guid>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoldingOrgs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HoldingOrgs_OrgGroups_FirstGuid",
                        column: x => x.FirstGuid,
                        principalTable: "OrgGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HoldingOrgs_Orgs_SecondGuid",
                        column: x => x.SecondGuid,
                        principalTable: "Orgs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrgDepartaments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    FirstGuid = table.Column<Guid>(nullable: false),
                    SecondGuid = table.Column<Guid>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgDepartaments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgDepartaments_Orgs_FirstGuid",
                        column: x => x.FirstGuid,
                        principalTable: "Orgs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrgDepartaments_Departaments_SecondGuid",
                        column: x => x.SecondGuid,
                        principalTable: "Departaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrgValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgValues_Orgs_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "Orgs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrgValues_OrgStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "OrgStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DepartamentPersons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    FirstGuid = table.Column<Guid>(nullable: false),
                    SecondGuid = table.Column<Guid>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartamentPersons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartamentPersons_Departaments_FirstGuid",
                        column: x => x.FirstGuid,
                        principalTable: "Departaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepartamentPersons_Persons_SecondGuid",
                        column: x => x.SecondGuid,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonValues_Persons_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonValues_PersonStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "PersonStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostValues_Posts_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostValues_PostStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "PostStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    FirstGuid = table.Column<Guid>(nullable: false),
                    SecondGuid = table.Column<Guid>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonUsers_Persons_FirstGuid",
                        column: x => x.FirstGuid,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonUsers_Users_SecondGuid",
                        column: x => x.SecondGuid,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    CreateAuthor = table.Column<Guid>(nullable: true),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    DateTimeOffsetValue = table.Column<DateTimeOffset>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    GuidValue = table.Column<Guid>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    ObjectGuid = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StaticGuid = table.Column<Guid>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateAuthor = table.Column<Guid>(nullable: true),
                    UpdateDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserValues_Users_ObjectGuid",
                        column: x => x.ObjectGuid,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserValues_UserStatics_StaticGuid",
                        column: x => x.StaticGuid,
                        principalTable: "UserStatics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DepartamentPersons_FirstGuid",
                table: "DepartamentPersons",
                column: "FirstGuid");

            migrationBuilder.CreateIndex(
                name: "IX_DepartamentPersons_SecondGuid",
                table: "DepartamentPersons",
                column: "SecondGuid");

            migrationBuilder.CreateIndex(
                name: "IX_DepartamentValues_ObjectGuid",
                table: "DepartamentValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_DepartamentValues_StaticGuid",
                table: "DepartamentValues",
                column: "StaticGuid");

            migrationBuilder.CreateIndex(
                name: "IX_HoldingOrgs_FirstGuid",
                table: "HoldingOrgs",
                column: "FirstGuid");

            migrationBuilder.CreateIndex(
                name: "IX_HoldingOrgs_SecondGuid",
                table: "HoldingOrgs",
                column: "SecondGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgDepartaments_FirstGuid",
                table: "OrgDepartaments",
                column: "FirstGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgDepartaments_SecondGuid",
                table: "OrgDepartaments",
                column: "SecondGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgGroupValues_ObjectGuid",
                table: "OrgGroupValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgGroupValues_StaticGuid",
                table: "OrgGroupValues",
                column: "StaticGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgValues_ObjectGuid",
                table: "OrgValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_OrgValues_StaticGuid",
                table: "OrgValues",
                column: "StaticGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PersonUsers_FirstGuid",
                table: "PersonUsers",
                column: "FirstGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PersonUsers_SecondGuid",
                table: "PersonUsers",
                column: "SecondGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PersonValues_ObjectGuid",
                table: "PersonValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PersonValues_StaticGuid",
                table: "PersonValues",
                column: "StaticGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PostValues_ObjectGuid",
                table: "PostValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_PostValues_StaticGuid",
                table: "PostValues",
                column: "StaticGuid");

            migrationBuilder.CreateIndex(
                name: "IX_UserValues_ObjectGuid",
                table: "UserValues",
                column: "ObjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_UserValues_StaticGuid",
                table: "UserValues",
                column: "StaticGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DepartamentPersons");

            migrationBuilder.DropTable(
                name: "DepartamentValues");

            migrationBuilder.DropTable(
                name: "HoldingOrgs");

            migrationBuilder.DropTable(
                name: "OrgDepartaments");

            migrationBuilder.DropTable(
                name: "OrgGroupValues");

            migrationBuilder.DropTable(
                name: "OrgValues");

            migrationBuilder.DropTable(
                name: "PersonUsers");

            migrationBuilder.DropTable(
                name: "PersonValues");

            migrationBuilder.DropTable(
                name: "PostValues");

            migrationBuilder.DropTable(
                name: "UserValues");

            migrationBuilder.DropTable(
                name: "DepartamentStatics");

            migrationBuilder.DropTable(
                name: "Departaments");

            migrationBuilder.DropTable(
                name: "OrgGroups");

            migrationBuilder.DropTable(
                name: "OrgGroupStatics");

            migrationBuilder.DropTable(
                name: "Orgs");

            migrationBuilder.DropTable(
                name: "OrgStatics");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "PersonStatics");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "PostStatics");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "UserStatics");
        }
    }
}
