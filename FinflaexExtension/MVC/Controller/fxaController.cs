﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinflaexExtension.DataBase.Base;
using FinflaexExtension.MVC.Authorization;
using FinflaexExtension.Types;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace FinflaexExtension.MVC.Controller
{
    public class FxaController<TThis, TDb, TAuth>
        : Microsoft.AspNetCore.Mvc.Controller
        where TThis : FxaController<TThis, TDb, TAuth>
        where TDb : fxaDbContext<TDb>
        where TAuth : FxaAuthorization
    {
        public readonly IMemoryCache cache;
        private readonly IHostingEnvironment _environment;

        public FxaController(IMemoryCache cache)
        {
            this.cache = cache;
        }

        public FxaController(IMemoryCache cache
            , IHostingEnvironment environment)
        {
            this.cache = cache;
            this._environment = environment;
        }

        public IActionResult action_result(
            Action<ActionInjector<TDb, TAuth>> act)
        {
            Result(act);
            return new EmptyResult();
        }

        public async Task<IActionResult> action_result(
            Func<ActionInjector<TDb, TAuth>, Task> act)
        {
            await Result(act);
            return new EmptyResult();
        }

        public IActionResult function_result(
            Func<ActionInjector<TDb, TAuth>, IActionResult> act)
        {
            return Result(act);
        }

        public async Task<IActionResult> function_result(
            Func<ActionInjector<TDb, TAuth>, Task<IActionResult>> act)
        {
            return await Result(act);
        }

        public IActionResult json_result(
            Func<ActionInjector<TDb, TAuth>, object> act)
        {
            var read = Result(act);
            return Json(read);
        }

        public async Task<IActionResult> json_result<TR>(
            Func<ActionInjector<TDb, TAuth>, Task<TR>> act)
        {
            var read = await Result(act);
            return Json(read);
        }

        public void Result(Action<ActionInjector<TDb, TAuth>> act)
        {
            var session = HttpContext.GetSession();
            var auth = cache.GetModel<TAuth>(session);
            using (var db = Activator.CreateInstance<TDb>())
            {
                db.AuthorGuid = SessionToUserGuid?.Invoke(db,session) ?? (auth?.Id ?? Guid.Empty);
                act(new ActionInjector<TDb, TAuth>
                {
                    Db = db,
                    SessionId = session,
                    Auth = auth,
                    AuthId = auth?.Id
                });
            }
        }

        public async Task Result(Func<ActionInjector<TDb, TAuth>, Task> act)
        {
            var session = HttpContext.GetSession();
            var auth = cache.GetModel<TAuth>(session);
            using (var db = Activator.CreateInstance<TDb>())
            {
                db.AuthorGuid = SessionToUserGuid?.Invoke(db,session) ?? (auth?.Id ?? Guid.Empty);
                await act(new ActionInjector<TDb, TAuth>
                {
                    Db = db,
                    SessionId = session,
                    Auth = auth,
                    AuthId = auth?.Id
                });
            }
        }

        public TR Result<TR>(Func<ActionInjector<TDb, TAuth>, TR> func)
        {
            var session = HttpContext.GetSession();
            var auth = cache.GetModel<TAuth>(session);
            using (var db = Activator.CreateInstance<TDb>())
            {
                db.AuthorGuid = SessionToUserGuid?.Invoke(db,session) ?? (auth?.Id ?? Guid.Empty);
                return func(new ActionInjector<TDb, TAuth>
                {
                    Db = db,
                    SessionId = session,
                    Auth = auth,
                    AuthId = auth?.Id
                });
            }
        }

        public async Task<TR> Result<TR>(Func<ActionInjector<TDb, TAuth>, Task<TR>> func)
        {
            var session = HttpContext.GetSession();
            var auth = cache.GetModel<TAuth>(session);
            using (var db = Activator.CreateInstance<TDb>())
            {
                db.AuthorGuid = SessionToUserGuid?.Invoke(db, session) ?? (auth?.Id ?? Guid.Empty);
                return await func(new ActionInjector<TDb, TAuth>
                {
                    Db = db,
                    SessionId = session,
                    Auth = auth,
                    AuthId = auth?.Id
                });
            }
        }

        public static Func<TDb, Guid?, Guid> SessionToUserGuid = null;
    }

    public class ActionInjector<TDb, TAuth>
        where TDb : fxaDbContext<TDb>
        where TAuth : FxaAuthorization
    {
        public ActionInjector()
        {
            Dict = new DictSo();
        }

        public DateTimeOffset Now => Db?.CurrentTime ?? DateTimeOffset.Now;

        public TDb Db { get; set; }
        public Guid? SessionId { get; set; }
        public TAuth Auth { get; set; }
        public Guid? AuthId { get; set; }
        public Guid? ValueId { get; set; }
        public DictSo Dict { get; set; }
        public Guid? ValueId2 { get; set; }

        public ActionInjector<TDb, TAuth> Add<TValue>(
            TValue value)
            where TValue : class
        {
            Db?.Add(value);
            return this;
        }

        public ActionInjector<TDb, TAuth> Remove<TValue>(
            TValue value)
            where TValue : class
        {
            Db?.Remove(value);
            return this;
        }

        public ActionInjector<TDb, TAuth> AddRange<TValue>(
            IEnumerable<TValue> value)
            where TValue : class
        {
            Db?.AddRange(value);
            return this;
        }

        public ActionInjector<TDb, TAuth> Commit()
        {
            Db?.Commit();
            return this;
        }

        public async Task CommitAsync()
        {
            if (Db == null) return;
            await Db.CommitAsync();
        }
    }
}
