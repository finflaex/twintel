﻿using System;
using System.Collections.Generic;

namespace FinflaexExtension.MVC.Authorization
{
    public class Roles : Attribute
    {
        private readonly List<string> _roles = new List<string>();

        public Roles()
        {
            
        }

        public Roles(params string[] roles) : this()
        {
            _roles.AddRange(roles);
        }

        public IEnumerable<string> Values => _roles;
    }
}
