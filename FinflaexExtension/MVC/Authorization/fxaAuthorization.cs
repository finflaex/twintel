﻿using System;
using System.Collections.Generic;

namespace FinflaexExtension.MVC.Authorization
{
    public abstract class FxaAuthorization
    {
        public IEnumerable<string> Roles { get; set; }
        public string Caption { get; set; }
        public Guid Id { get; set; }
    }
}
