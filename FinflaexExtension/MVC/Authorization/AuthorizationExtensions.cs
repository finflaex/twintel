﻿using System;
using FinflaexExtension.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace FinflaexExtension.MVC.Authorization
{
    public static class AuthorizationExtensions
    {
        //public static async Task SignInAsync<T>(
        //    this HttpContext @this,
        //    T record)
        //    where T : fxaAuthorization
        //{
        //    var claims = record
        //        .Roles
        //        .Select(role => new Claim(ClaimTypes.Role, role))
        //        .Append(new Claim(ClaimTypes.Name, record.Caption))
        //        .Append(new Claim(typeof(T).Name, record.ToJson(), ""));

        //    var id = new ClaimsIdentity(
        //        claims,
        //        "ApplicationCookie",
        //        ClaimTypes.Name,
        //        ClaimTypes.Role);

        //    await @this.SignInAsync("Cookies", new ClaimsPrincipal(id));
        //}

        //public static async Task SignOutAsync(
        //    this HttpContext @this)
        //{
        //    await @this.SignOutAsync("Cookies");
        //}

        //public static T GetAuthRecord<T>(
        //    this Controller controller)
        //    where T : fxaAuthorization
        //    => controller
        //           .HttpContext?.User.Claims
        //           .FirstOrDefault(v => v.Type == typeof(T).Name)?
        //           .Value?.FromJson<T>() ?? Fxr.Create<T>();

        //public static T Author<T>(
        //    this ViewContext context)
        //    where T : fxaAuthorization
        //{
        //    return context
        //               .HttpContext?.User.Claims
        //               .FirstOrDefault(v => v.Type == typeof(T).Name)?
        //               .Value?.FromJson<T>() ?? Fxr.Create<T>();
        //}


        public static Guid? GetSession(this HttpContext @this)
        {
            return @this.Request.Cookies.TryGetValue("session", out var rawSession)
                ? rawSession.ToNullableGuid()
                : null;
        }

        public static T GetModel<T>(this IMemoryCache @this, Guid? guid)
            where T : FxaAuthorization
        {
            return guid == null ? null : @this.Get<T>(guid);
        }

        public static T GetAuthModel<T>(this IMemoryCache @this, HttpContext context)
            where T : FxaAuthorization
        {
            var session = context.GetSession();
            return session == null ? null : @this.Get<T>(session);
        }

        public static HttpContext SetSession(
            this HttpContext @this, 
            Guid? guid,
            TimeSpan? time = null)
        {
            @this.Response.Cookies.Delete("session");
            @this.Response.Cookies.Append("session", guid.RetIfNull(Guid.Empty)?.ToString("N"), new CookieOptions
            {
                Expires = DateTimeOffset.Now.Add(time ?? new TimeSpan(0, 20, 0))
            });
            return @this;
        }

        public static IMemoryCache SetModel<T>(this IMemoryCache @this, 
            Guid? guid, 
            T model, 
            TimeSpan? time = null)
            where T : FxaAuthorization
        {
            if (guid != null)
            {
                @this.Set(guid, model, time ?? new TimeSpan(0, 20, 0));
            }
            return @this;
        }
    }
}
