﻿namespace FinflaexExtension.Types
{
    public class DictOo : FxaDict<object, object>
    {
        public object Get(object key) => this[key];
    }
}
