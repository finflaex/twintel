﻿using System.Collections.Generic;

namespace FinflaexExtension.Types
{
    public class FxaDict<TKey, TVal> : Dictionary<TKey, TVal>
    {
        public new TVal this[TKey key]
        {
            get => ContainsKey(key) ? base[key] : default(TVal);
            set => base[key] = value;
        }
    }
}
