﻿using FinflaexExtension.Reflection;
using Newtonsoft.Json;

namespace FinflaexExtension.Types
{
    public class DictSo : DictOo
    {
        public static implicit operator string(DictSo value) => value.Json;
        public static implicit operator DictSo(string value) => new DictSo(value);

        public DictSo()
        {
            
        }

        public DictSo(string value)
        {
            Json = value;
        }

        public T Get<T>(string key)
        {
            var read = Get(key);
            switch (read)
            {
                case null: return default(T);
                case T value: return value;
                default: return read.AsType<T>();
            }
        }

        public string GetString(string key)
        {
            var read = Get(key);
            switch (read)
            {
                case null: return null;
                case string value: return value;
                default: return read.ToString();
            }
        }

        [JsonIgnore]
        public string Json
        {
            get => this.ToJson();
            set
            {
                if (!value.IsFree())
                {
                    try
                    {
                        JsonConvert.PopulateObject(value, this, new JsonSerializerSettings
                        {
                            TypeNameHandling = TypeNameHandling.All,
                            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full,
                            Formatting = Formatting.Indented,
                            MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
                        });
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
        }

        public T GetObject<T>(T value = null)
            where T : class
        {
            var read = value.CreateIfNull();
            JsonConvert.PopulateObject(Json, read);
            return read;
        }

        public DictSo SetObject<T>(T value)
        {
            Json = value.ToJson();
            return this;
        }


        public static DictSo Create(string read) => read;
        public static DictSo Create(object read) => new DictSo().SetObject(read);
    }
}
