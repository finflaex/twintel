﻿using System;
using System.Threading.Tasks;
using FinflaexExtension.DataBase.Table.Interfaces;
using FinflaexExtension.Reflection;
//using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace FinflaexExtension.DataBase.Base
{
    public abstract class fxaDbContext<TThis>
        : DbContext, fxiDbContext
        where TThis : fxaDbContext<TThis>
    {
        public Guid AuthorGuid = Guid.Empty;

        protected virtual void Worker()
        {
            ChangeTracker
                .Entries()
                .DynamicForEach(entry =>
                {
                    if (entry.Entity is fxiCreateTable
                        && entry.State == EntityState.Added)
                        entry.Entity.AsType<fxiCreateTable>().Act(value =>
                        {
                            value.CreateAuthor = AuthorGuid;
                            value.CreateDate = CurrentTime;
                        });

                    if (entry.Entity is fxiUpdateTable
                        && (entry.State == EntityState.Added || entry.State == EntityState.Modified))
                        entry.Entity.AsType<fxiUpdateTable>().Act(value =>
                        {
                            value.UpdateAuthor = AuthorGuid;
                            value.UpdateDate = CurrentTime;
                        });

                    if (entry.Entity is fxiStartEndTable)
                        entry.Entity.AsType<fxiStartEndTable>().Act(value =>
                        {
                            if (value.Start == null) value.Start = DateTimeOffset.MinValue;
                            if (value.End == null) value.End = DateTimeOffset.MaxValue;
                        });
                });
        }

        #region SYSTEM

        //public HttpContext Context { get; set; }

        public DateTimeOffset CurrentTime
            => CurrenTimeSpan == null
                ? DateTimeOffset.Now
                : DateTimeOffset.Now.Add(CurrenTimeSpan.Value);

        public TimeSpan? CurrenTimeSpan { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder db)
        {
            db.UseSqlServer(ConnectionString);
        }

        #endregion

        #region METHODS

        public TThis Commit()
        {
            Worker();
            SaveChanges();
            return this.AsType<TThis>();
        }

        public async Task<TThis> CommitAsync()
        {
            lock (this)
            {
                Worker();
            }
            await SaveChangesAsync();
            return this.AsType<TThis>();
        }

        public async Task<TThis> Transaction(Func<Task> act = null)
        {
            using (var t = await GetTransaction)
            {
                try
                {
                    if (act != null)
                    {
                        await act.Invoke();
                    }
                    t.Commit();
                }
                catch (Exception error)
                {
                    t.Rollback();
                    throw error;
                }
            }
            return this.AsType<TThis>();
        }

        public Task<IDbContextTransaction> GetTransaction => Database.BeginTransactionAsync();

        public string ConnectionString { get; set; }

        #endregion
    }
}