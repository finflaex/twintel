namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiDoubleValueTable
    {
        double DoubleValue { get; set; }
    }
}