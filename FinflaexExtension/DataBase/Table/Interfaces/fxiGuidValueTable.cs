using System;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiGuidValueTable
    {
        Guid GuidValue { get; set; }
    }
}