﻿namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiRawValueTable
    {
        byte[] RawValue { get; set; }
    }
}
