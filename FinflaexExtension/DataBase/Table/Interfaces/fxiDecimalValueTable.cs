namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiDecimalValueTable
    {
        decimal DecimalValue { get; set; }
    }
}