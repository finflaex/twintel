#region

using System;
using System.Collections.Generic;

#endregion

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiHierarchyTable<T> : fxiGuidTable
        where T : fxiHierarchyTable<T>
    {
        Guid? ParentId { get; set; }
        T Parent { get; set; }
        ICollection<T> Childs { get; set; }
    }
}