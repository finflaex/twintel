#region

#endregion

using System.ComponentModel.DataAnnotations;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiIdTable<TKey>
    {
        [Key]
        TKey Id { get; set; }
    }
}