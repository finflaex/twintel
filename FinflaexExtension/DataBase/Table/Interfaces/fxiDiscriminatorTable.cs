﻿using System.ComponentModel.DataAnnotations;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiDiscriminatorTable
    {
        [Display(Name = "Класс")]
        string Discriminator { get; set; }
    }
}
