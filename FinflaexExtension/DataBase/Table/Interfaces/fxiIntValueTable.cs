namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiIntValueTable
    {
        int IntValue { get; set; }
    }
}