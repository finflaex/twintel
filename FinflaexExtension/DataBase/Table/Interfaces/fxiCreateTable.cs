#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiCreateTable
    {
        [Display(Name = "���� ��������")]
        DateTimeOffset CreateDate { get; set; }

        [Display(Name = "���������")]
        Guid? CreateAuthor { get; set; }
    }
}