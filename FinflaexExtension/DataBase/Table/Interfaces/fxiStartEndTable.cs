﻿#region

using System;

#endregion

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiStartEndTable
    {
        DateTimeOffset? Start { get; set; }
        DateTimeOffset? End { get; set; }
    }
}