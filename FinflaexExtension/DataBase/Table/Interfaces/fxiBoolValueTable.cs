namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiBoolValueTable
    {
        bool BoolValue { get; set; }
    }
}