#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiUpdateTable
    {
        [Display(Name = "���� �����������")]
        DateTimeOffset UpdateDate { get; set; }

        [Display(Name = "��������")]
        Guid? UpdateAuthor { get; set; }
    }
}