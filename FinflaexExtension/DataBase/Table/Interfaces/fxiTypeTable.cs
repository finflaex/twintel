using System.ComponentModel.DataAnnotations;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiTypeTable<TType>
    {
        [Display(Name = "Тип")]
        TType Type { get; set; }
    }
}