namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiStringValueTable
    {
        string StringValue { get; set; }
    }
}