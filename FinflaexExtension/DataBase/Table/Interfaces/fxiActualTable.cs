﻿using System.ComponentModel;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiActualTable
    {
        [DisplayName("Актуальность")]
        bool Actual { get; set; }
    }
}
