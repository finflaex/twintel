using System;

namespace FinflaexExtension.DataBase.Table.Interfaces
{
    public interface fxiDateTimeOffsetValueTable
    {
        DateTimeOffset DateTimeOffsetValue { get; set; }
    }
}