﻿#region

using System;
using FinflaexExtension.DataBase.Table.Interfaces;

#endregion

namespace FinflaexExtension.DataBase.Table.Abstracts
{
    public abstract class fxaGuidTable : fxiGuidTable
    {
        protected fxaGuidTable()
        {
            Id = Guid.NewGuid();
        }

        public virtual Guid Id { get; set; }
    }
}