﻿using System;
using System.Globalization;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static DateTime ToDateTime(this string @this, string mask = null, DateTime? defaultValue = null)
        {
            try
            {
                if (mask == null)
                    return DateTime.Parse(@this);
                return DateTime.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return defaultValue ?? DateTime.MinValue;
            }
        }

        public static TimeSpan ToTimeSpan(this string @this, string mask = null, TimeSpan? defaultValue = null)
        {
            try
            {
                if (mask == null) return TimeSpan.Parse(@this);
                return TimeSpan.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return defaultValue ?? TimeSpan.MinValue;
            }
        }
    }
}
