﻿using System;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static TThis ThrowIfNull<TThis>(
            this TThis @this
            , string message = null)
        {
            if (@this == null)
            {
                throw new Exception(message ?? string.Empty);
            }
            return @this;
        }

        public static TThis ThrowIfNull<TThis>(
            this TThis @this,
            Exception err)
        {
            if (@this == null)
            {
                throw err;
            }
            return @this;
        }
    }
}
