﻿using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static string InString(this IHtmlContent @this)
        {
            var writer = new System.IO.StringWriter();
            @this.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }

        public static IHtmlContent InContent(this string @this)
        {
            return new HtmlString(@this);
        }

        public static IHtmlContent TrimScript(this IHtmlContent @this)
        {
            return @this
                .InString()
                .Trim()
                .RemoveStart("<script>")
                .RemoveEnd("</script>")
                .InContent();
        }
    }
}
