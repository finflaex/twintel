﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static bool IsDebug(this IHtmlHelper htmlHelper)
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        public static bool IsDebug<T>(this IHtmlHelper<T> htmlHelper)
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        public static IHtmlContent ToRaw(
            this string @this)
        {
            return new HtmlString(@this);
        }
    }
}
