﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static void DynamicForEach<T>(
            this IEnumerable<T> @this,
            Action<T> action)
        {
            foreach (var item in @this)
                action(item);
        }

        public static void StaticForEach<T>(
            this IEnumerable<T> @this,
            Action<T> action)
        {
            var array = @this.ToArray();
            foreach (var item in array)
                action(item);
        }

        public static IEnumerable<T> ActIfAny<T>(
            this IEnumerable<T> @this,
            Action<IEnumerable<T>> act)
        {
            var actIfAny = @this as T[] ?? @this.ToArray();
            if (actIfAny.Any())
            {
                act(actIfAny);
            }
            return actIfAny;
        }
        
        public static IEnumerable<T> ActIfEmpty<T>(
            this IEnumerable<T> @this,
            Action act)
        {
            var actIfEmpty = @this as T[] ?? @this.ToArray();
            if (!actIfEmpty.Any())
            {
                act();
            }
            return actIfEmpty;
        }
    }
}