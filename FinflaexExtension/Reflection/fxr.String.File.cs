﻿using System.IO;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static string FileExt(
            this string @this)
        {
            return Path
                .GetExtension(@this)
                .TrimStart('.')
                .ToLower();
        }

        public static bool FileExt(
            this string @this,
            string ext)
        {
            return Path
                .GetExtension(@this)
                .TrimStart('.')
                .ToLower()
                .Equals(ext.ToLower());
        }

        public static string FileName(
            this string @this)
        {
            return Path
                .GetFileNameWithoutExtension(@this)
                .ToLower();
        }
    }
}
