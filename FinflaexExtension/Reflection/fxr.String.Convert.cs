﻿using System;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static string ToBaseNumber(this long @this, int basa)
        {
            var input = @this;
            string result = string.Empty;
            while (input != 0)
            {
                var rem = input % basa;
                input = input / basa;
                if (rem > 9)
                {
                    rem = rem + 'A' - 10;
                    result = (char) rem + result;
                }
                else
                {
                    result = rem + result;
                }
            }
            return result;
        }

        public static long FromBaseNumber(this string @this, int basa)
        {
            long result = 0;
            var length = 0;
            for (int i = @this.Length - 1; i >= 0; i--)
            {
                if (@this[i] >= 'A' && @this[i] <= 'Z')
                    result = (int)(result + (@this[i] - 55) * Math.Pow(basa, length));
                else
                    result = (int)(result + (@this[i] - '0') * Math.Pow(basa, length));
                length++;
            }
            return result;
        }
    }
}
