﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static string EncodeStringUnicode(this string @this)
        {
            return Regex.Unescape(@this);
        }

        public static string UrlDecode(this string self, Encoding encoding = null)
        {
            if (encoding == null)
                return HttpUtility.UrlDecode(self);
            return HttpUtility.UrlDecode(self, encoding);
        }

        public static string UrlEncode(this string self, Encoding encoding = null)
        {
            if (encoding == null)
                return HttpUtility.UrlEncode(self);
            return HttpUtility.UrlEncode(self, encoding);

        }

        public static string HtmlDecode(this string self)
        {
            return HttpUtility
                .HtmlDecode(self);
        }

        public static string HtmlEncode(this string self)
        {
            return HttpUtility
                .HtmlEncode(self);
        }

        public static string Encrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] results;
                var utf8 = new UTF8Encoding();
                var hashProvider = new MD5CryptoServiceProvider();
                var tdesKey = hashProvider.ComputeHash(utf8.GetBytes(password));
                var tdesAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = tdesKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var dataToEncrypt = utf8.GetBytes(value);
                try
                {
                    var encryptor = tdesAlgorithm.CreateEncryptor();
                    results = encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
                }
                finally
                {
                    tdesAlgorithm.Clear();
                    hashProvider.Clear();
                }
                return Convert.ToBase64String(results);
            }
            catch
            {
                return "";
            }
        }

        public static string Decrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] results;
                var utf8 = new UTF8Encoding();
                var hashProvider = new MD5CryptoServiceProvider();
                var tdesKey = hashProvider.ComputeHash(utf8.GetBytes(password));
                var tdesAlgorithm = new TripleDESCryptoServiceProvider();
                tdesAlgorithm.Key = tdesKey;
                tdesAlgorithm.Mode = CipherMode.ECB;
                tdesAlgorithm.Padding = PaddingMode.PKCS7;
                var dataToDecrypt = Convert.FromBase64String(value);
                try
                {
                    var decryptor = tdesAlgorithm.CreateDecryptor();
                    results = decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length);
                }
                finally
                {
                    tdesAlgorithm.Clear();
                    hashProvider.Clear();
                }
                return utf8.GetString(results);
            }
            catch
            {
                return "";
            }
        }

        public static string ToBase64(this string self)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(self));
        }

        public static string ToJson(this object @this, bool format = false)
        {
            return JsonConvert.SerializeObject(@this, format ? Formatting.Indented : Formatting.None);
        }

        public static T FromJson<T>(this string @this)
        {
            return @this.IsFree() ? default(T) : JsonConvert.DeserializeObject<T>(@this);
        }

        public static object FromJson(this string @this)
        {
            return JsonConvert.DeserializeObject(@this);
        }
    }
}