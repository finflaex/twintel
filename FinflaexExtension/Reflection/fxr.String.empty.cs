﻿using System;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        /// <summary>
        /// ret if IsNullOrWhiteSpace
        /// </summary>
        /// <param name="this"></param>
        /// <param name="iffree"></param>
        /// <param name="else"></param>
        /// <returns></returns>
        public static string RetifFree(
            this string @this,
            string iffree,
            Func<string,string> @else)
        {
            return @this.RetifFree(iffree, @else(@this));
        }

        public static string RetifFree(
            this string @this,
            string iffree = "",
            string @else = null)
        {
            if (string.IsNullOrWhiteSpace(@this))
            {
                return iffree;
            }
            if (@else == null)
            {
                return @this;
            }
            return @else;
        }

        public static string RetIf(
            this string @this,
            string @if,
            string @return)
        {
            return @this.Equals(@if, StringComparison.InvariantCultureIgnoreCase)
                ? @return
                : @this;
        }

        public static bool IsFree(
            this string @this)
        {
            return string.IsNullOrWhiteSpace(@this);
        }
    }
}
