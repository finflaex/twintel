﻿using System;
using System.Threading.Tasks;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static void Act<T>(this T @this, Action<T> act)
        {
            act(@this);
        }

        public static TR Func<T,TR>(this T @this, Func<T,TR> act)
        {
            return act(@this);
        }

        public static T ActWhere<T>(this T @this, Func<T, bool> where, Action<T> act)
        {
            if (where(@this))
            {
                act(@this);
            }
            return @this;
        }

        public static T FuncWhere<T>(this T @this, Func<T, bool> where, Func<T, T> act)
        {
            if (where(@this))
            {
                return act(@this);
            }
            return @this;
        }

        public static T ActIfNull<T>(this T @this, Action act)
        {
            if (@this == null)
            {
                act();
            }
            return @this;
        }

        public static T ActIfNotNull<T>(this T @this, Action<T> act)
        {
            if (@this != null)
            {
                act(@this);
            }
            return @this;
        }

        public static TR Ret<T, TR>(this T @this, Func<T, TR> act)
        {
            return act.Invoke(@this);
        }

        public static TR RetIfNull<TR>(this TR @this, TR defaultValue)
        {
            if (@this == null)
                return defaultValue;
            return @this;
        }

        public static TR RetIfNull<TR>(this TR @this, Func<TR> act)
            where TR: class 
        {
            return @this ?? act();
        }

        public static R RetIfNotNull<T,R>(this T @this, Func<T, R> act)
        {
            return @this != null ? act(@this) : default(R);
        }

        public static Task<TR> AsAsync<TR>(this Func<TR> act)
        {
            var tcs = new TaskCompletionSource<TR>();
            Task.Run(() =>
            {
                try
                {
                    var result = act();
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public static Task<TR> AsAsync<T1,TR>(this Func<T1, TR> act, T1 value1)
        {
            var tcs = new TaskCompletionSource<TR>();
            Task.Run(() =>
            {
                try
                {
                    var result = act(value1);
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public static Task<TR> AsAsync<T1, T2, TR>(this Func<T1, T2, TR> act, T1 value1, T2 value2)
        {
            var tcs = new TaskCompletionSource<TR>();
            Task.Run(() =>
            {
                try
                {
                    var result = act(value1, value2);
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public static Task<TR> AsAsync<T1, T2, T3, TR>(this Func<T1, T2, T3, TR> act, T1 value1, T2 value2, T3 value3)
        {
            var tcs = new TaskCompletionSource<TR>();
            Task.Run(() =>
            {
                try
                {
                    var result = act(value1, value2, value3);
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public static T[] InArray<T>(
            this T @this)
        {
            return new [] {@this};
        }
    }
}