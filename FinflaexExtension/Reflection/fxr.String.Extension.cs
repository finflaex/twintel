﻿using System.Linq;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        /// <summary>
        /// очистить кроме
        /// </summary>
        /// <param name="this"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static string ClearExcept(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => list.Contains(v)).ToArray().Ret(v => new string(v));
        }

        /// <summary>
        /// Очистить от
        /// </summary>
        /// <param name="this"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static string Clear(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => !list.Contains(v)).ToArray().Ret(v => new string(v));
        }

        public static string RemoveStart(this string @this, string value)
        {
            var count = value.Length;
            if (@this.Length > count)
            {
                var control = @this.Substring(0, count);
                if (control == value)
                {
                    return @this.Substring(count);
                }
            } else if (@this.Length == count)
            {
                return string.Empty;
            }
            return @this;
        }

        public static string RemoveEnd(this string @this, string value)
        {
            var count = value.Length;
            var length = @this.Length;
            if (length < count) return @this;

            var control = @this.Substring(length - count);
            if (control == value)
            {
                return @this.Substring(0, length - count);
            }
            return @this;
        }

        public static bool IsWhiteSpace(
            this string @this)
        {
            return string.IsNullOrWhiteSpace(@this);
        }

        public static bool NotWhiteSpace(
            this string @this)
            => !@this.IsWhiteSpace();

        public static bool IsWhiteSpace(
            this object @this)
            => @this.ToString().IsWhiteSpace();

        public static bool NotWhiteSpace(
            this object @this)
            => @this.ToString().NotWhiteSpace();
    }
}
