﻿using System;
using FinflaexExtension.Types;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static TR AsType<TR>(this object @this)
        {
            try
            {
                return (TR)@this;
            }
            catch
            {
                return default(TR);
            }
        }

        public static T Create<T>(params object[] param)
        {
            return Activator
                .CreateInstance(typeof(T), param)
                .AsType<T>();
        }

        public static T CreateIfNull<T>(
            this T @this,
            Action<T> actIfCreate = null)
        {
            if (@this == null)
            {
                var result = Activator.CreateInstance<T>();
                actIfCreate?.Invoke(result);
                return result;
            }
            return @this;
        }

        public static T CreateIfNull<T,TR>(
            this T @this,
            Func<T,TR> actIfCreate = null)
        {
            if (@this == null)
            {
                var result = Activator.CreateInstance<T>();
                actIfCreate?.Invoke(result);
                return result;
            }
            return @this;
        }

        public static DictSo ToDictSo<T>(this T @this)
        {
            return new DictSo().SetObject(@this);
        }
    }
}
