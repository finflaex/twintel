﻿using System;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static Guid ToGuid(
          this int @this,
          int subId = 0,
          int level1 = 0,
          int level2 = 0,
          int level3 = 0)
        {
            return new Guid($"{subId:00000000}-{level1:0000}-{level2:0000}-{level3:0000}-{@this:000000000000}");
        }

        public static int GetInt(this Guid @this, int defaultValue = 0)
            => @this.ToString("D").Split('-')[4].ToTryInt(defaultValue);

        public static int GetSubInt(this Guid @this, int defaultValue = 0)
            => @this.ToString("D").Split('-')[0].ToTryInt(defaultValue);

        public static int GetIntLevel1(this Guid @this, int defaultValue = 0)
            => @this.ToString("D").Split('-')[1].ToTryInt(defaultValue);

        public static int GetIntLevel2(this Guid @this, int defaultValue = 0)
            => @this.ToString("D").Split('-')[2].ToTryInt(defaultValue);

        public static int GetIntLevel3(this Guid @this, int defaultValue = 0)
            => @this.ToString("D").Split('-')[3].ToTryInt(defaultValue);

        public static Guid OutInt(this Guid @this, out int value, int defaultValue = 0)
        {
            value = @this.GetInt(defaultValue);
            return @this;
        }

        public static Guid OutSubInt(this Guid @this, out int value, int defaultValue = 0)
        {
            value = @this.GetSubInt(defaultValue);
            return @this;
        }


        public static Guid OutIntLevel1(this Guid @this, out int value, int defaultValue = 0)
        {
            value = @this.GetIntLevel1(defaultValue);
            return @this;
        }


        public static Guid OutIntLevel2(this Guid @this, out int value, int defaultValue = 0)
        {
            value = @this.GetIntLevel2(defaultValue);
            return @this;
        }


        public static Guid OutIntLevel3(this Guid @this, out int value, int defaultValue = 0)
        {
            value = @this.GetIntLevel3(defaultValue);
            return @this;
        }

        public static bool IsNullOrEmpty(this Guid? @this)
        {
            return @this == null || @this == Guid.Empty;
        }

        public static bool IsEmpty(this Guid @this)
        {
            return @this == Guid.Empty;
        }

        public static bool NotNullOrEmpty(this Guid? @this)
        {
            return @this != null && @this != Guid.Empty;
        }

        public static bool NotEmpty(this Guid @this)
        {
            return @this != Guid.Empty;
        }

        public static Guid ToGuid(this string @this, Guid? defaultValue = null)
        {
            try
            {
                return Guid.Parse(@this);
            }
            catch
            {
                return defaultValue ?? Guid.Empty;
            }
        }

        public static Guid? ToNullableGuid(this string @this, Guid? defaultValue = null)
        {
            if (@this.IsFree())
            {
                return defaultValue;
            }
            try
            {
                var read = Guid.Parse(@this);
                if (read.IsEmpty())
                {
                    return defaultValue;
                }
                return read;
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
