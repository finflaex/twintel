﻿using System;
using System.Globalization;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        /// <summary>
        /// int.Parse
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static int ToInt(this string @this)
        {
            return int.Parse(@this);
        }

        /// <summary>
        /// Всевозможные проверки и очистка от других символов
        /// </summary>
        /// <param name="this"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToTryInt(this string @this, int defaultValue = 0)
        {
            var clear = $"{@this}".ClearExcept("0123456789");
            if (clear.Length != 0)
            {
                var result = int.Parse(@this);
                return result == 0 ? defaultValue : result;
            }
            return defaultValue;
        }

        public static float ToFloat(this string @this, float defaultValue = 0)
        {
            try
            {
                var result = float.Parse(@this.Replace(',', '.'), CultureInfo.InvariantCulture);
                return Math.Abs(result) < 0.000000000001 ? defaultValue : result;
            }
            catch
            {
                return defaultValue;
            }
        }

        public static double ToDouble(this string @this, double defaultValue = 0)
        {
            try
            {
                var result = double.Parse(@this.Replace(',', '.'), CultureInfo.InvariantCulture);
                return Math.Abs(result) < 0.000000000001 ? defaultValue : result;
            }
            catch
            {
                return defaultValue;
            }
        }

        public static long ToLong(this string @this, long defaultValue = 0)
        {
            try
            {
                var result = long.Parse(@this);
                return result == 0 ? defaultValue : result;
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
