﻿using System.Linq;
using System.Text.RegularExpressions;

namespace FinflaexExtension.Reflection
{
    public static partial class Fxr
    {
        public static Match[] DoRegexMatches(
            this string @this, 
            string pattern, 
            RegexOptions options = RegexOptions.None)
        {
            return @this.DoRegexIsMatch(pattern, options) 
                ? new Regex(pattern, options)
                .Matches(@this)
                .Cast<Match>()
                .ToArray()
                : new Match[0];
        }

        public static bool DoRegexIsMatch(
            this string @this, 
            string pattern, 
            RegexOptions options = RegexOptions.None)
        {
            return !@this.IsWhiteSpace() && Regex.IsMatch(@this, pattern, options);
        }
    }
}
